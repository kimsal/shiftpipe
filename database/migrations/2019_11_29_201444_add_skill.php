<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSkill extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skill', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('account_id');
            $table->string('name');
            $table->string('type'); // text or number
            $table->timestamps();
        });
        Schema::create('skill_user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('user_id');
            $table->uuid('skill_id');
            $table->integer('numeric_value')->nullable();
            $table->string('string_value')->nullable();
            $table->text('descripion')->nullable();
            $table->timestamps();
        });
        Schema::create('skill_shift', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('shift_id');
            $table->uuid('skill_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skill');
    }
}
