<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('location', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('account_id')->nullable(); // for testing may not want this just yet
            $table->string('name');
            $table->string('timezone')->default("America/New_York");
            $table->uuid('address_id')->nullable();
            $table->text('description')->nullable();
            $table->text('notes')->nullable(); // internal
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('location');
    }
}
