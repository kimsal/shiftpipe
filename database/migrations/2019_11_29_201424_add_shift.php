<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddShift extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shift', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('location_id');
            $table->uuid('worker_id')->nullable();
            $table->uuid('poster_id')->nullable();
            $table->uuid('approver_id')->nullable();
            $table->datetime('wallclock_start'); // future start
            $table->datetime('wallclock_end')->nullable(); // future end
            $table->integer('occurred')->nullable(); // when occurred (past)
            $table->string('status')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shift');
    }
}
