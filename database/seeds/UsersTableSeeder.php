<?php

use App\Repositories\SuperUserRepository;
use App\Services\AccountService;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // some time later, make this random?
        $password = "super";
        $repo = new SuperUserRepository();
        $user = $repo->create(
            [
                'email'=>'super@admin.com',
                'name'=>"Super Admin",
                'password'=>$password
            ]
        );
        echo "Password for super@admin.com set to $password\n";

        // make sample login user and account
        $accountService = new AccountService();
        $sampleUser = $accountService->createAccountWithOwnerUser("user@user.com","user 1","user", "Sample Account");
    }
}
