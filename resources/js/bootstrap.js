window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

window.axios.defaults.headers.common['X-CSRF-TOKEN'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
// need to get the laravel api token for a user... and will need to add api_token to user...
window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + window.laravel.apiToken;
window.axios.defaults.baseURL = "/api";


// generic loading spinner on each api call
window.axiosCounter = 0;
const requestHandler = (request) => {
    window.axiosCounter++;
    if (window.axiosCounter > 0) {
        window.loaderApp.loading = true;
    }
    return request;
};

const errorHandler = (error) => {
    return Promise.reject({ ...error })
};

const successHandler = (response) => {
    return response
};


window.axios.interceptors.request.use(
    request => requestHandler(request)
);
window.axios.interceptors.response.use((response) => {
    window.axiosCounter--;
    if (window.axiosCounter == 0) {
        window.loaderApp.loading = false;
    }
    return successHandler(response);
}, (error) => {
    window.axiosCounter--;
    if (window.axiosCounter == 0) {
        window.loaderApp.loading = false;
    }
    return errorHandler(error);
});
