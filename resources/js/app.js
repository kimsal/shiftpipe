/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import ExampleComponent from "./components/ExampleComponent";

require('./bootstrap');

window.Vue = require('vue');
import BootstrapVue from 'bootstrap-vue'
import Loading from 'vue-loading-overlay'

Vue.use(BootstrapVue);

Vue.component('loading', require('vue-loading-overlay'));

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('account-location', require('./components/AccountLocation.vue').default);
Vue.component('shifts', require('./components/Shifts.vue').default);
Vue.component('my-shifts', require('./components/MyShifts.vue').default);
Vue.component('worker-admin', require('./components/WorkerAdmin.vue').default);
Vue.component('invited', require('./components/Invited.vue').default);
Vue.component('worker-profile', require('./components/WorkerProfile.vue').default);
Vue.prototype.$http = window.axios;

import VueTimepicker from 'vue2-timepicker'
Vue.component('vue-timepicker', require('vue2-timepicker').default);

import moment from 'moment'
Vue.prototype.moment = moment;

import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
Vue.use(VueSweetalert2);

/**
 * vue routers
 */
import VueRouter from 'vue-router'
import AccountLocation from "./components/AccountLocation";
import Shifts from "./components/Shifts";
import WorkerProfile from "./components/WorkerProfile";
import WorkerAdmin from "./components/WorkerAdmin";
import MyShifts from "./components/MyShifts";

Vue.use(VueRouter);

const routes = [
    // { path: '/', component: ExampleComponent },
    { path: '/workers', component: WorkerAdmin },
    { path: '/location', component: AccountLocation },
    { path: '/shifts', component: Shifts },
    { path: '/myshifts', component: MyShifts },
    { path: '/me', component: WorkerProfile },
];

const router = new VueRouter({
    routes // short for `routes: routes`
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


const app = new Vue({
    router,
    el: '#app',
});

window.vueApp = app;


const loaderApp = new Vue({
    router,
    el: '#loader',
    data: {
        loading: false,
    },
});

window.loaderApp = loaderApp;
