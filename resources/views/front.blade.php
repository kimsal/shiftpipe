@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
    <div class="col-md-12">
        <h2>Shift scheduling software</h2>
        <ul>
            <li>Manage your staff</li>
            <li>Handle multiple locations</li>
            <li>Easy email and SMS notifications</li>
            <li>Reporting</li>
        </ul>
    </div>
    </div>
</div>
@endsection
