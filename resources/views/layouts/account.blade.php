<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="loader">
        <loading
            ref="loading-spinner"
            :active="loading"
            :can-cancel="true"
            :is-full-page="true"></loading>
    </div>
    <div id="app">
        @include("layouts.nav")
        <main class="py-4">
            <div class="container">
                <div class="row">
                    <div class="col-md-2">
                        @include("layouts.account.sidenav")
                    </div>
                    <div class="col-md-10">
                        @yield('content')
                    </div>
                </div>
            </div>
        </main>
    </div>
    <script>
        window.laravel = {!! json_encode([
           'csrfToken' => csrf_token(),
           'apiToken' => Auth::user()->api_token ?? null,
       ]) !!};
    </script>
</body>
</html>
