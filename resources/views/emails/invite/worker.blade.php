@component('mail::message')
# Welcome

You've been invited to create an account with ShiftPipe.

@component('mail::button', ['url' => route('invitation_click', ['invitation'=>$invitation])])
Click here to create your account
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
