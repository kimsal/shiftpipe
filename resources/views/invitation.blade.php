@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <invited :invitation='{!! json_encode($invitation) !!}'/>
    </div>
</div>
@endsection
