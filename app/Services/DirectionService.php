<?php


namespace App\Services;


use App\Models\Address;
use Illuminate\Support\Facades\Config;
use UnitConverter\UnitConverter;

class DirectionService
{

    const TYPE_DRIVING = 'driving';
    const TYPE_WALKING = 'walking';
    const TYPE_TRAFFIC = 'driving-traffic';
    const TYPE_CYCLING = 'cycling';

    public $accessToken = '';
    public $type = self::TYPE_DRIVING;
    public $baseUrl = 'https://api.mapbox.com/directions/v5/mapbox/';

    public function __construct($accessToken = null)
    {
        $key = $accessToken ?? Config::get('directions.token');
        $this->accessToken = $key;
    }

    /**
     * @param Address $addressFrom
     * @param Address $addressTo
     * @return array
     */
    public function getDirections(Address $addressFrom, Address $addressTo) : array
    {
        $fromLatitude = $addressFrom->latitude;
        $fromLongitude = $addressFrom->longitude;
        $toLatitude = $addressTo->latitude;
        $toLongitude = $addressTo->longitude;
        $url = $this->baseUrl;
        $url .= $this->type."/";
        $url .= $fromLatitude.','.$fromLongitude .';';
        $url .= $toLatitude.','.$toLongitude;
        $url .= '.json';
        $url .= '?access_token='.$this->accessToken;

        $results = file_get_contents($url);
        $out = $this->parseResults($results);

        return $out;
    }

    /**
     * We'll assume one leg - point A to point B
     * https://docs.mapbox.com/api/navigation/#directions
     *
     * duration is seconds
     * distance is meters
     *
     * @param $results
     * @return array
     */
    public function parseResults($results) : array
    {
        $json = json_decode($results);
        $routes = $json->routes;
        $route = $routes[0];
        $distance = $route->distance;
        $duration = $route->duration;

        $converter = UnitConverter::createBuilder()
            ->addSimpleCalculator()
            ->addDefaultRegistry()
            ->build();

        $dist = [];
        $miles = round($converter->convert($distance)->from("m")->to("mi"),1);
        $milesString = $miles .' miles';
        $dist['meters'] = $distance;
        $dist['miles'] = $miles;
        $dist['string'] = $milesString;

        $timeHours = 0;
        $timeMinutes = (int)round($converter->convert($duration)->from('s')->to('min'));
        if($timeMinutes>60) {
            $timeHours = floor($converter->convert($duration)->from('s')->to('hr'));
            $newDuration = $duration - ($timeHours * 3600);
            $timeMinutes = (int)round($converter->convert($newDuration)->from('s')->to('min'));
        }
        $time = [];
        $time['seconds'] = $duration;
        $time['hours'] = $timeHours;
        $time['minutes'] = $timeMinutes;
        $timeString = ($timeHours>0) ? ($timeHours) . ' hours ' : ' ';
        $timeString .= $timeMinutes . ' minutes';
        $time['string'] = trim($timeString);

        $return = [];
        $return['time'] = $time;
        $return['distance'] = $dist;

        return $return;

    }
}
