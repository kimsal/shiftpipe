<?php

namespace App\Services;

use App\Models\Address;
use App\Models\Distance;
use Illuminate\Support\Collection;
use UnitConverter\UnitConverter;

class DistanceService
{

    /**
     * @param Address $address1
     * @param Address $address2
     * @return Distance
     */
    public function addDistance(Address $address1, Address $address2)
    {
        $service = new DirectionService();
        $output = $service->getDirections($address1, $address2);
        $distance = new Distance();
        $distance->address_from_id = $address1->id;
        $distance->address_to_id = $address2->id;
        $distance->meters = $output['distance']['meters'];
        $distance->seconds = $output['time']['seconds'];
        $distance->save();
        return $distance;
    }

    /**
     * @param Address $address
     * @param float $seconds - time seconds
     * @return Collection<Address>
     */
    public function findAddressesBySeconds(Address $address, float $seconds): Collection
    {
        $builder = Distance::where('address_from_id', $address->id);
        $builder->where('seconds', '<=', $seconds);
        $builder->with('targetAddress');
        $addresses = $builder->get()->pluck("targetAddress");
        return $addresses;
    }

    /**
     * @param Address $address
     * @param float $meters
     * @return Collection<Address>
     */
    public function findAddressesByMeters(Address $address, float $meters): Collection
    {
        $builder = Distance::where('address_from_id', $address->id);
        $builder->where('meters', '<=', $meters);
        $builder->with('targetAddress');
        $addresses = $builder->get()->pluck("targetAddress");
        return $addresses;
    }

    /**
     * @param Address $address
     * @param float $miles
     * @return Collection<Address>
     */
    public function findAddressesByMiles(Address $address, float $miles): Collection
    {
        $converter = UnitConverter::createBuilder()
            ->addSimpleCalculator()
            ->addDefaultRegistry()
            ->build();

        $meters = round($converter->convert($miles)->from("mi")->to("m"),4);

        return $this->findAddressesByMeters($address, $meters);
    }

}
