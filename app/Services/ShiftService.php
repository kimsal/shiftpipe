<?php


namespace App\Services;


use App\Models\Account;
use App\Models\Location;
use App\Models\Shift;
use App\StorableEvents\ShiftCreated;
use App\Models\User;
use App\Repositories\ShiftRepository;
use App\StorableEvents\ShiftDeleted;
use App\StorableEvents\ShiftTaken;
use App\StorableEvents\ShiftUntaken;
use App\StorableEvents\ShiftUpdated;
use Carbon\Carbon;
use Ulid\Ulid;

class ShiftService
{

    /** @var Account $account */
    public $account;

    public function __construct(Account $account)
    {
        $this->account = $account;
    }

    /**
     * @param Location $location
     * @param string $wallStartDate
     * @param string $wallStartTime
     * @param string $wallEndDate
     * @param string $wallEndTime
     * @param User $poster
     * @return Shift
     */
    public function schedule(
        string $location_id,
        string $wallStartDate,
        string $wallStartTime,
        string $wallEndDate,
        string $wallEndTime,
        User $poster
    ): Shift {
        $_wallclockStart = new Carbon($wallStartDate . " " . $wallStartTime);
        $wallclockStart = $_wallclockStart->format("Y-m-d H:i");
        $_wallclockEnd = new Carbon($wallEndDate . " " . $wallEndTime);
        $wallclockEnd = $_wallclockEnd->format("Y-m-d H:i");

        $id = Ulid::generate();
        event(
            new ShiftCreated(
                (string)$id, $this->account->id,
                $wallclockStart, $wallclockEnd, $poster->id, Shift::STATUS_OPEN, $location_id
            )
        );
        $shift = (new ShiftRepository($this->account))->get((string)$id);

        return $shift;
    }

    /**
     * @param string $shiftId
     * @param Location $location
     * @param string $wallStartDate
     * @param string $wallStartTime
     * @param string $wallEndDate
     * @param string $wallEndTime
     * @param string $status
     * @param User $poster
     * @return Shift
     */
    public function update(
        string $shiftId,
        string $location_id,
        string $wallStartDate,
        string $wallStartTime,
        string $wallEndDate,
        string $wallEndTime,
        User $poster
    ): Shift {
        $wallclockStart = $wallStartDate . " " . $wallStartTime;
        $wallclockEnd = $wallEndDate . " " . $wallEndTime;

        event(
            new ShiftUpdated(
                $shiftId, $this->account->id,
                $wallclockStart, $wallclockEnd, $poster->id, $location_id
            )
        );

        $shift = (new ShiftRepository($this->account))->get((string)$shiftId);

        return $shift;
    }

    /**
     * @param string $shiftId
     * @param string $workerId
     * @return Shift
     */
    public function take(string $shiftId, string $workerId, string $userId = null): Shift
    {
        event(new ShiftTaken($shiftId, $this->account->id, $workerId, $userId));
        $shift = (new ShiftRepository($this->account))->get($shiftId);
        return $shift;
    }

    /**
     * @param string $shiftId
     * @param string $workerId
     * @param string $userId
     * @return Shift
     */
    public function untake(string $shiftId, string $workerId, string $userId): Shift
    {
        event(new ShiftUntaken($shiftId, $this->account->id, $userId));
        $shift = (new ShiftRepository($this->account))->get($shiftId);
        return $shift;
    }

    /**
     * @param string $shiftId
     * @param User|null $user
     * @return void
     */
    public function delete(string $shiftId, User $user = null): void
    {
        event(new ShiftDeleted($shiftId, $this->account->id,
                             $user ? $user->id : null));
        return;
    }

}
