<?php


namespace App\Services;


use App\Models\Account;
use App\Models\Role;
use App\Models\User;
use App\Repositories\AccountRepository;
use App\Repositories\UserRepository;
use App\StorableEvents\AccountCreated;
use Illuminate\Support\Facades\Hash;
use Ulid\Ulid;

class AccountService
{

    /**
     * @param $email
     * @param $name
     * @param $password
     * @param $accountName
     * @return User
     */
    public function createAccountWithOwnerUser($email, $name, $password, $accountName): User
    {
        $accountId = Ulid::generate();
        $userId = Ulid::generate();
        $hashedPassword = Hash::make($password);
        event(new AccountCreated($accountId, $userId, $email, $name, $hashedPassword, $accountName));
        $account = (new AccountRepository())->get((string)$accountId);
        $user = (new UserRepository($account))->get((string)$userId);
        self::assignUserToAccountAsOwner($account, $user);
        return $user;
    }

    /**
     * @deprecated
     */
    public function assignUserToAccount($account, $user)
    {
        $user->account_id = $account->id;
        $user->save();
    }

    public function assignUserToAccountAsOwner($account, $user)
    {
        $user->account_id = $account->id;
        $role = Role::where('name', Role::ROLE_ACCOUNT_OWNER)->first();
        $user->assignRole($role);
        $user->save();
    }
}
