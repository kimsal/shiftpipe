<?php


namespace App\Services;


use App\Models\Account;
use App\Models\Address;
use App\Models\Role;
use App\Repositories\AddressRepository;
use App\StorableEvents\AddressCreated;
use App\StorableEvents\AddressUpdated;
use App\User;
use App\Repositories\AccountRepository;
use App\Repositories\UserRepository;
use App\StorableEvents\AccountCreated;
use Illuminate\Support\Facades\Hash;
use Ulid\Ulid;

class AddressService
{

    /**
     * @param $address1
     * @param $address2
     * @param $city
     * @param $state
     * @param $postal_code
     * @param $latitude
     * @param $longitude
     * @param $verified
     * @param $note
     * @return Address
     */
    static public function create(
        $address1 = null,
        $address2 = null,
        $city = null,
        $state = null,
        $postal_code = null,
        $latitude = null,
        $longitude = null,
        $verified = null,
        $note = null
    ): Address {
        $id = (string)Ulid::generate();
        event(new AddressCreated($id, $address1, $address2, $city, $state, $postal_code, $latitude, $longitude, $verified, $note));
        $address = (new AddressRepository())->get((string)$id);
        return $address;
    }

    /**
     * @param $address1
     * @param $address2
     * @param $city
     * @param $state
     * @param $postal_code
     * @param $latitude
     * @param $longitude
     * @param $verified
     * @param $note
     * @return Address
     */
    static public function update(Address $address): Address
    {
        event(new AddressUpdated($address->id, $address->address1, $address->address2,
                                 $address->city, $address->state, $address->postal_code,
                                 $address->latitude, $address->longitude,
                                 $address->verified, $address->note));
        $address = (new AddressRepository())->get((string)$address->id);
        return $address;
    }

    static public function updateByArray(array $address): Address
    {
        event(new AddressUpdated($address['id'], $address['address1'], @$address['address2'],
                                 $address['city'], $address['state'], $address['postal_code'],
                                 $address['latitude'], $address['longitude'],
                                 @$address['verified'], @$address['note']));
        $address = (new AddressRepository())->get((string)$address['id']);
        return $address;
    }

    static public function createByArray(array $address): Address
    {
        $id = (string)Ulid::generate();
        event(new AddressCreated($id, $address['address1'], @$address['address2'],
                                 $address['city'], $address['state'], $address['postal_code'],
                                 $address['latitude'], $address['longitude'],
                                 @$address['verified'], @$address['note']));
        $address = (new AddressRepository())->get((string)$id);
        return $address;
    }


}
