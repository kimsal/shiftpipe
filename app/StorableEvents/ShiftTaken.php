<?php

namespace App\StorableEvents;

use Spatie\EventSourcing\ShouldBeStored;

final class ShiftTaken implements ShouldBeStored
{
    public $shiftId;
    public $accountId;
    public $userId;
    public $workerId;

    public function __construct($shiftId, $accountId, $workerId, $userId = null)
    {
        $this->shiftId = $shiftId;
        $this->accountId = $accountId;
        $this->workerId = $workerId;
        $this->userId = $userId;
    }
}
