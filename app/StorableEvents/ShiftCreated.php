<?php

namespace App\StorableEvents;

use App\Models\Shift;
use App\Repositories\ShiftRepository;
use Spatie\EventSourcing\ShouldBeStored;

final class ShiftCreated implements ShouldBeStored
{

    public $shift_id;
    public $wallclock_start;
    public $wallclock_end;
    public $poster_id;
    public $status;
    public $location_id;
    public $account_id;

    public function __construct(
        $shift_id,
        $account_id,
        $wallclock_start,
        $wallclock_end,
        $poster_id,
        $status,
        $location_id
    ) {
        $this->shift_id = $shift_id;
        $this->account_id = $account_id;
        $this->wallclock_start = $wallclock_start;
        $this->wallclock_end = $wallclock_end;
        $this->poster_id = $poster_id;
        $this->status = $status;
        $this->location_id = $location_id;
    }


}
