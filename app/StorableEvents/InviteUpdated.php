<?php

namespace App\StorableEvents;

use Spatie\EventSourcing\ShouldBeStored;

final class InviteUpdated implements ShouldBeStored
{
    public function __construct()
    {
    }
}
