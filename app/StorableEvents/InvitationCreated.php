<?php

namespace App\StorableEvents;

use App\Models\Worker;
use Illuminate\Support\Facades\DB;
use Spatie\EventSourcing\ShouldBeStored;
use Ulid\Ulid;

final class InvitationCreated implements ShouldBeStored
{
    public $inviteId;
    public $accountId;
    public $workerId;
    public $email;
    public $code;
    public $status;

    public function __construct($inviteId, $accountId, $workerId, $email, $code, $status)
    {
        $this->workerId = $workerId;
        $this->accountId = $accountId;
        $this->inviteId = $inviteId;
        $this->email = $email;
        $this->code = $code;
        $this->status = $status;
    }
}
