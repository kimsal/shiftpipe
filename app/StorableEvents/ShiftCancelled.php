<?php

namespace App\StorableEvents;

use Spatie\EventSourcing\ShouldBeStored;

final class ShiftCancelled implements ShouldBeStored
{
    public function __construct()
    {
    }
}
