<?php

namespace App\StorableEvents;

use Spatie\EventSourcing\ShouldBeStored;

final class ShiftDeleted implements ShouldBeStored
{

    public $shiftId;
    public $accountId;
    public $userId;

    public function __construct($shiftId, $accountId, $userId = null)
    {
        $this->shiftId = $shiftId;
        $this->accountId = $accountId;
        $this->userId = $userId;
    }
}
