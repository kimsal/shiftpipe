<?php

namespace App\StorableEvents;

use Spatie\EventSourcing\ShouldBeStored;

final class WorkerCreated implements ShouldBeStored
{
    public $workerId;
    public $accountId;
    public $posterId;
    public $name;
    public $email;
    public $phone;
    public $status;
    public $workerData;

    public function __construct($workerId, $accountId, $workerData, $posterId = null)
    {
        $this->workerId = $workerId;
        $this->accountId = $accountId;
        $this->name = @$workerData['name'];
        $this->email = @$workerData['email'];
        $this->phone = @$workerData['phone'];
        $this->status = @$workerData['status'];
        $this->posterId = $posterId;
    }
}
