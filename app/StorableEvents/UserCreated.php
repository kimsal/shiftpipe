<?php

namespace App\StorableEvents;

use Illuminate\Support\Facades\Hash;
use Spatie\EventSourcing\ShouldBeStored;

final class UserCreated implements ShouldBeStored
{

    public $id;
    public $name;
    public $accountId;
    public $email;
    public $hashedPassword;

    public function __construct($id, $accountId, $name, $email, $hashedPassword = null, $rawPassword=null)
    {
        $this->id = $id;
        $this->accountId = $accountId;
        $this->email = $email;
        $this->name = $name;
        // hash the password here?
        $this->hashedPassword = $hashedPassword;
        if($rawPassword != null)
        {
            $this->hashedPassword = Hash::make($rawPassword);
        }
    }
}
