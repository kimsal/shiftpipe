<?php

namespace App\StorableEvents;

use Spatie\EventSourcing\ShouldBeStored;

final class ShiftUpdated implements ShouldBeStored
{
    public $shift_id;
    public $wallclock_start;
    public $wallclock_end;
    public $poster_id;
    public $location_id;
    public $account_id;

    public function __construct(
        $shift_id,
        $account_id,
        $wallclock_start,
        $wallclock_end,
        $poster_id,
        $location_id
    ) {
        $this->shift_id = $shift_id;
        $this->account_id = $account_id;
        $this->wallclock_start = $wallclock_start;
        $this->wallclock_end = $wallclock_end;
        $this->poster_id = $poster_id;
        $this->location_id = $location_id;
    }
}
