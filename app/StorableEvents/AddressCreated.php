<?php

namespace App\StorableEvents;

use Spatie\EventSourcing\ShouldBeStored;

final class AddressCreated implements ShouldBeStored
{
    public $id = null;
    public $address1 = null;
    public $address2 = null;
    public $city = null;
    public $state = null;
    public $postal_code = null;
    public $latitude = null;
    public $longitude = null;
    public $verified = null;
    public $note = null;

    public function __construct(
        string $id,
        string $address1 = null,
        string $address2 = null,
        string $city = null,
        string $state = null,
        string $postal_code = null,
        string $latitude = null,
        string $longitude = null,
        string $verified = null,
        string $note = null
    ) {
        $this->id = (string)$id;
        $this->address1 = $address1;
        $this->address2 = $address2;
        $this->city = $city;
        $this->state = $state;
        $this->postal_code = $postal_code;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->verified = $verified;
        $this->note = $note;
    }
}
