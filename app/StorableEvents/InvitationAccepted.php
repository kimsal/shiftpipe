<?php

namespace App\StorableEvents;

use Spatie\EventSourcing\ShouldBeStored;

final class InvitationAccepted implements ShouldBeStored
{

    public $invitationId;
    public $accountId;
    public $email;
    public $name;
    public $hashedPassword;
    public $userId;

    public function __construct($invitationId, $accountId, $email, $name, $hashedPassword, $userId = null)
    {
        $this->invitationId = $invitationId;
        $this->accountId = $accountId;
        $this->email = $email;
        $this->name = $name;
        $this->hashedPassword = $hashedPassword;
        $this->userId = $userId;
    }
}
