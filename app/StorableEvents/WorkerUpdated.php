<?php

namespace App\StorableEvents;

use Spatie\EventSourcing\ShouldBeStored;

final class WorkerUpdated implements ShouldBeStored
{
    /**
     * @var string
     */
    public $accountId;
    /**
     * @var string
     */
    public $workerId;
    /**
     * @var array
     */
    public $workerData;

    public function __construct(string $accountId, string $workerId, array $workerData)
    {
        $this->accountId = $accountId;
        $this->workerId = $workerId;
        $this->workerData = $workerData;
    }
}
