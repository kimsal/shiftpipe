<?php

namespace App\StorableEvents;

use Spatie\EventSourcing\ShouldBeStored;

final class ShiftChanged implements ShouldBeStored
{
    public function __construct()
    {
    }
}
