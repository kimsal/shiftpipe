<?php

namespace App\StorableEvents;

use Spatie\EventSourcing\ShouldBeStored;

final class AccountCreated implements ShouldBeStored
{
    public $accountId;
    public $userId;
    public $email;
    public $name;
    public $hashedPassword;
    public $accountName;

    /**
     * AccountCreated constructor.
     * @param string $accountId
     * @param string $userId
     * @param string $email
     * @param string $name
     * @param string $hashedPassword
     * @param string $accountName
     */
    public function __construct(string $accountId, string $userId,
                                string $email, string $name,
                                string $hashedPassword, string $accountName)
    {
        $this->accountId =$accountId;
        $this->userId = $userId;
        $this->email = $email;
        $this->name = $name;
        $this->hashedPassword = $hashedPassword;
        $this->accountName = $accountName;
    }
}
