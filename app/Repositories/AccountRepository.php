<?php

namespace App\Repositories;

use App\Models\Account;
use Illuminate\Database\Eloquent\Collection;

class AccountRepository implements RepositoryInterface
{

    /**
     * @return Collection<Account>
     */
    public function all(): Collection {

    }

    public function create(array $data): Account
    {
        return Account::create($data);
    }

    public function update(array $data, $id): Account
    {
        $location = Account::find($id);
        return $location->update($data);
    }

    public function delete($id)
    {
        $location = Account::find($id);
        $location->delete();
    }

    public function get($id): Account
    {
        $a =  Account::where('id',$id)->first();
        return $a;
    }

    public function firstWhere($field, $value, $operator = '='): Account
    {
        return Account::where($field, $operator, $value)->first();
    }

}
