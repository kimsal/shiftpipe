<?php

namespace App\Repositories;

use App\Models\Account;
use App\Models\Location;
use Illuminate\Database\Eloquent\Collection;

class LocationRepository implements RepositoryInterface
{

    public $account;

    public function __construct(Account $account)
    {
        $this->account = $account;
    }

    /**
     * @return Collection<Location>
     */
    public function all(): Collection
    {
        $locations = Location::where('account_id', $this->account->id)
                        ->with('address')->get();
        return $locations;
    }

    public function create(array $data): Location
    {
        $data['account_id'] = $this->account->id;
        return Location::create($data);
    }

    public function update(array $data, $id): Location
    {
        $location = Location::where('account_id', $this->account->id)->where('id', $id)->first();
        $location->update($data);
        return $location;
    }

    public function delete($id)
    {
        $location = Location::where('account_id', $this->account->id)->where('id', $id)->first();
        $location->delete();
    }

    public function get($id): Location
    {
        $location = Location::where('account_id', $this->account->id)->where('id', $id)->first();
        return $location;
    }
}
