<?php

namespace App\Repositories;

use App\Models\Account;
use App\Models\Location;
use App\Models\Shift;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class ShiftRepository implements RepositoryInterface
{

    /** @var Account */
    public $account;

    public function __construct(Account $account = null)
    {
        $this->account = $account;
    }

    public function baseBuilder(): Builder
    {
        $accountId = $this->account->id;
        $builder = Shift::join("location", function ($join) use ($accountId) {
            $join->on('shift.location_id', '=', 'location.id')
                ->where('location.account_id', '=', $accountId);
        })->select(['shift.*']);
        return $builder;
    }

    /**
     * @return Collection<Shift>
     */
    public function all(): Collection
    {
        $builder = self::baseBuilder();
        $shifts = $builder->get();
        return $shifts;
    }

    public function allWithLocations(): Collection
    {
        $builder = self::baseBuilder();
        $shifts = $builder->with(['location'])->get();
        return $shifts;
    }


    public function create(array $data): Shift
    {
        return Shift::create($data);
    }

    public function update(array $data, $id): Shift
    {
        $shift = Shift::find($id);
        $shift->update($data);
        return $shift;
    }

    public function delete($id)
    {
        $shift = Shift::find($id);
        $shift->delete();
    }

    public function get($id): Shift
    {
        $builder = self::baseBuilder();
        /** @var Shift $shift */
        $shift = $builder->where('shift.id', $id)->first();
        return $shift;
    }

    /**
     * Find shifts that start at or after date/time string combination
     *
     * @param $dateTimeString "2019-09-19 07:00:00"
     */
    public function _getAfterBuilder($dateTimeString, $builderClause = null): Builder
    {
        $builder = self::baseBuilder();
        $shiftQuery = $builder->where('wallclock_start', '>=', $dateTimeString);
        return $shiftQuery;
    }

    /**
     * Find shifts that start at or before date/time string combination
     * @param $dateTimeString
     * @return Collection<Shift>
     */
    public function getAfter($dateTimeString): Collection
    {
        $shiftQuery = self::_getAfterBuilder($dateTimeString);
        $shifts = $shiftQuery->get();
        return $shifts;
    }

    public function getAfterAtLocation($dateTimeString, Location $location): Collection
    {
        $shiftQuery = self::_getAfterBuilder($dateTimeString);
        $shiftQuery->where('location_id', '=', $location->id);
        $shifts = $shiftQuery->get();
        return $shifts;
    }

    /**
     * @param $dateTimeString
     * @return Builder
     */
    protected function _getBeforeBuilder($dateTimeString): Builder
    {
        $builder = self::baseBuilder();
        $shiftQuery = $builder->where('wallclock_start', '<=', $dateTimeString);
        return $shiftQuery;
    }

    /**
     * Find shifts that start at or before date/time string combination
     * @param $dateTimeString
     * @return Collection<Shift>
     */
    public function getBefore($dateTimeString): Collection
    {
        $shiftQuery = self::_getBeforeBuilder($dateTimeString);
        $shifts = $shiftQuery->get();
        return $shifts;
    }

    public function getBeforeAtLocation($dateTimeString, Location $location): Collection
    {
        $shiftQuery = self::_getBeforeBuilder($dateTimeString);
        $shiftQuery->where('location_id', '=', $location->id);
        $shifts = $shiftQuery->get();
        return $shifts;
    }

    /**
     * Find shifts that start between two date/time string combinations
     * @param $dateTimeStartString
     * @param $dateTimeEndString
     * @return Builder
     */
    public function _getBetweenBuilder($dateTimeStartString, $dateTimeEndString): Builder
    {
        $builder = self::baseBuilder();
        $shiftQuery = $builder->where('wallclock_start', '>=', $dateTimeStartString)
            ->where('wallclock_end', '<=', $dateTimeEndString);
        return $shiftQuery;
    }

    /**
     * Find shifts that start between two date/time string combinations
     * @param $dateTimeStartString
     * @param $dateTimeEndString
     * @return Collection<Shift>
     */
    public function getBetween($dateTimeStartString, $dateTimeEndString): Collection
    {
        $shiftQuery = self::_getBetweenBuilder($dateTimeStartString, $dateTimeEndString);
        $shifts = $shiftQuery->get();
        return $shifts;
    }


    /**
     * Find shifts that start between two date/time string combinations
     * and match given status
     * @param $dateTimeStartString
     * @param $dateTimeEndString
     * @return Collection<Shift>
     */
    public function getBetweenByStatus($dateTimeStartString, $dateTimeEndString, string $status): Collection
    {
        $shiftQuery = self::_getBetweenBuilder($dateTimeStartString, $dateTimeEndString);
        $shiftQuery->where('status', $status);
        $shifts = $shiftQuery->get();
        return $shifts;
    }


    /**
     * Find shifts that start between two date/time string combinations
     * @param $dateTimeStartString
     * @param $dateTimeEndString
     * @return Collection<Shift>
     */
    public function getBetweenAtLocation($dateTimeStartString, $dateTimeEndString, Location $location): Collection
    {
        $shiftQuery = self::_getBetweenBuilder($dateTimeStartString, $dateTimeEndString);
        $shiftQuery->where('location_id', '=', $location->id);
        $shifts = $shiftQuery->get();
        return $shifts;
    }

}
