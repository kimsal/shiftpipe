<?php

namespace App\Repositories;

use App\Mail\WorkerInvite;
use App\Models\Account;
use App\Models\Invitation;
use App\Models\Worker;
use App\StorableEvents\InvitationCreated;
use App\StorableEvents\WorkerCreated;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules\In;
use Ulid\Ulid;

class WorkerRepository implements RepositoryInterface
{

    public $account;

    public function __construct(Account $account)
    {
        $this->account = $account;
    }

    /**
     * @return Collection<Worker>
     */
    public function all(): Collection
    {
        $items = Worker::where('account_id', $this->account->id)->get();
        return $items;
    }

    public function createAndInvite(array $workerData): Invitation
    {
//        DB::beginTransaction();
        $workerId = Ulid::generate();
        $workerData['status'] = Worker::STATUS_INVITED;
        event(new WorkerCreated((string)$workerId, $this->account->id, $workerData, null));
        $worker = (new WorkerRepository($this->account))->get((string)$workerId);
        $inviteId = (string)Ulid::generate();
        $workerData['code'] = $workerData['code'] ?? Str::random(60);
        $inviteId = Ulid::generate();
        event(new InvitationCreated((string)$inviteId, $this->account->id, (string)$workerId, $workerData['email'], $workerData['code'], Invitation::STATUS_SENT));
        $invitation = (new InvitationRepository($this->account))->get((string)$inviteId);
//        DB::commit();
        return $invitation;
    }

    /**
     * @deprecated
     * @param Worker $worker
     * @return Invitation
     * @throws \Exception
     */
    public function invite(Worker $worker): Invitation
    {
        $repo = new InvitationRepository($this->account);
        $invitation = $repo->create(['email'=>$worker->email, 'worker_id'=>$worker->id]);
        return $invitation;
    }

    /**
     * @deprecated
     * mailing handling in invitationReactor
     * @param Invitation $invitation
     */
    public function sendInvite(Invitation $invitation)
    {
        Mail::to($invitation->worker->email)->queue(new WorkerInvite($invitation));
    }


    public function create(array $data): Worker
    {
        $data['account_id'] = $this->account->id;
        $worker = Worker::create($data);
        return $worker;
    }

    public function update(array $data, $id): Worker
    {
        $item = Worker::where('account_id', $this->account->id)->where('id', $id)->first();
        $item->update($data);
        return $item;
    }

    public function delete($id)
    {
        $item = Worker::where('account_id', $this->account->id)->where('id', $id)->first();
        $item->delete();
    }

    public function get($id): Worker
    {
        $item = Worker::where('account_id', $this->account->id)->where('id', $id)->first();
        return $item;
    }
}
