<?php

namespace App\Repositories;

use App\Models\Account;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserRepository implements RepositoryInterface
{

    /** @var Account $account */
    public $account;

    public function __construct(Account $account)
    {
        $this->account = $account;
    }

    public function all(): Collection
    {

    }

    public function create(array $data): User
    {
        $data['password'] = $data['hashedPassword'] ?? Hash::make($data['password']);
        $data['api_token'] = Str::random(70);
        unset($data['hashedPassword']);
        return User::create($data);
    }

    public function createWithRole(array $data, string $roleName): User
    {
        $user = self::create($data);
        $role = Role::where('name', $roleName)->first();
        $user->assignRole($role);
        return $user;
    }

    public function update(array $data, $id): User
    {
        $user = User::find($id);
        if (array_key_exists("password", $data)) {
            $data['password'] = Hash::make($data['password']);
        }
        return $user->update($data);
    }

    public function delete($id)
    {
        $user = User::find($id);
        $user->delete();
    }

    public function get($id): User
    {
        return User::find($id);
    }


}
