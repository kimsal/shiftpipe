<?php

namespace App\Repositories;

use App\Models\Address;
use Illuminate\Database\Eloquent\Collection;

class AddressRepository implements RepositoryInterface
{

    /**
     * @return Collection<Address>
     */
    public function all(): Collection {

    }

    public function create(array $data): Address
    {
        return Address::create($data);
    }

    public function update(array $data, $id): Address
    {
        $address = Address::find($id);
        $address->update($data);
        return $address;
    }

    public function delete($id)
    {
        $location = Address::find($id);
        $location->delete();
    }

    public function get($id) : Address
    {
        return Address::where('id',$id)->first();
    }

    public function firstWhere($field, $value, $operator = '='): Address
    {
        return Address::where($field, $operator, $value)->first();
    }

}
