<?php

namespace App\Repositories;

use App\Models\Account;
use App\Models\Invitation;
use App\Models\Role;
use App\Models\User;
use App\Models\Worker;
use App\StorableEvents\InvitationAccepted;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Ulid\Ulid;

class InvitationRepository implements RepositoryInterface
{

    public $account;

    public function __construct(Account $account)
    {
        $this->account = $account;
    }

    /**
     * @return Collection<Invitation>
     */
    public function all(): Collection
    {
        $items = Invitation::leftJoin('invitation', function($join) {
            $join->on('worker.id', '=', 'invitation.worker_id');
        }) ->where('worker.account_id',$this->account->id)->get();
        return $items;
    }


    /**
     * @param array $data
     * @return Invitation
     * @throws \Exception
     */
    public function create(array $data): Invitation
    {
        // does this account tie to the worker_id in question?
        $w = Worker::find($data['worker_id']);
        if(!$w || $w->account_id != $this->account->id)
        {
            throw new \Exception("bad or missing worker");
        }
        $data['status'] = $data['status'] ?? Invitation::STATUS_CREATED;
        $data['email'] = $data['email'] ?? $w->email;
        $invitation = Invitation::create($data);
        return $invitation;
    }

    public function update(array $data, $id): Invitation
    {
        $item = Invitation::where('account_id', $this->account->id)->where('id', $id)->first();
        $item->update($data);
        return $item;
    }

    public function delete($id)
    {
        $item = Invitation::leftJoin('invitation', function($join) {
            $join->on('worker.id', '=', 'invitation.worker_id');
        }) ->where('worker.account_id',$this->account->id)->where('id', $id)->first();
        $item->delete();
    }

    /**
     * @param $id
     * @return Invitation
     * @throws \Exception
     */
    public function get($id): Invitation
    {
        $item = Invitation::find($id);
//        if(!$item) { dd("no invitation found for $id"); }
        if($item->worker->account_id != $this->account->id)
        {
            throw new \Exception("accounts don't match");
        }
        return $item;
    }

    /**
     * User should be user who performed the acceptance, if not a self-acceptance? (rare?)
     * not dealt with in event projectors right now, but can be stored in case.
     *
     * $data - incoming name/email/password from invitation signup process
     * we will make a base user account from this but need to hash up front
     * because it will be stored
     *
     * @param Invitation $invitation
     * @param array $data
     * @param User|null $user
     */
    public function accept(Invitation $invitation, array $data, User $user = null)
    {
        $data['hashedPassword'] = Hash::make($data['password']);
        event(new InvitationAccepted($invitation->id, $this->account->id, $data['email'] ,$data['name'], $data['hashedPassword'], $user->id ?? null));
    }
}
