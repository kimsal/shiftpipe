<?php

namespace App\Projectors;

use App\Models\Invitation;
use App\Models\Role;
use App\Models\Worker;
use App\Repositories\AccountRepository;
use App\Repositories\InvitationRepository;
use App\Repositories\UserRepository;
use App\Repositories\WorkerRepository;
use App\StorableEvents\InvitationAccepted;
use App\StorableEvents\InvitationCreated;
use App\StorableEvents\UserCreated;
use App\StorableEvents\WorkerUpdated;
use Illuminate\Support\Facades\DB;
use Spatie\EventSourcing\Projectors\Projector;
use Spatie\EventSourcing\Projectors\ProjectsEvents;
use Ulid\Ulid;

final class InvitationProjector implements Projector
{
    use ProjectsEvents;

    public function onInvitationCreated(InvitationCreated $event)
    {
        $account = (new AccountRepository())->get($event->accountId);
        $repo = new InvitationRepository($account);
        $data = [];
        $data['id'] = $event->inviteId;
        $data['email'] = $event->email;
        $data['code'] = $event->code;
        $data['status'] = $event->status;
        $data['worker_id'] = $event->workerId;
        $repo->create($data);
    }

    public function onInvitationAccepted(InvitationAccepted $event)
    {
        DB::beginTransaction();
        $account = (new AccountRepository())->get($event->accountId);
        $invitation = (new InvitationRepository($account))->get($event->invitationId);

        $userId = Ulid::generate();
        event(new UserCreated((string)$userId, $event->accountId, $event->name, $event->email, $event->hashedPassword));
        $user = (new UserRepository($account))->get((string)$userId);
        $invitation->status = Invitation::STATUS_ACTIVE;
        $invitation->save();
        $update = [];
        $update['status'] = Worker::STATUS_ACTIVE;
        $update['user_id'] = $user->id;
        event(new WorkerUpdated($event->accountId, $invitation->worker_id, $update));
        $role = Role::where('name', Role::ROLE_WORKER)->first();
        $user->assignRole($role);
        $user->save();
        DB::commit();
    }
}
