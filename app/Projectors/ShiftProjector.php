<?php

namespace App\Projectors;

use App\Models\Account;
use App\Models\Shift;
use App\Repositories\ShiftRepository;
use App\StorableEvents\ShiftCancelled;
use App\StorableEvents\ShiftCreated;
use App\StorableEvents\ShiftDeleted;
use App\StorableEvents\ShiftTaken;
use App\StorableEvents\ShiftUntaken;
use App\StorableEvents\ShiftUpdated;
use Spatie\EventSourcing\Projectors\Projector;
use Spatie\EventSourcing\Projectors\ProjectsEvents;

final class ShiftProjector implements Projector
{
    use ProjectsEvents;

    public function onShiftCreated(ShiftCreated $event)
    {
        // should we use repo here?

        $account = Account::find($event->account_id);
        $shiftRepo = new ShiftRepository($account);
        $shiftRepo->create(
            [
                'id' => $event->shift_id,
                'location_id' => $event->location_id,
                'wallclock_start' => $event->wallclock_start,
                'wallclock_end' => $event->wallclock_end,
                'poster_id' => $event->poster_id,
                'status' => $event->status
            ]
        );
    }

    public function onShiftUpdate(ShiftUpdated $event)
    {
        $account = Account::find($event->account_id);
        $shiftRepo = new ShiftRepository($account);
        $shiftRepo->update(
            [
                'location_id' => $event->location_id,
                'wallclock_start' => $event->wallclock_start,
                'wallclock_end' => $event->wallclock_end,
                'poster_id' => $event->poster_id,
            ],
            $event->shift_id
        );
    }

    public function onShiftDelete(ShiftDeleted $event)
    {
        $account = Account::find($event->accountId);
        $shiftRepo = new ShiftRepository($account);
        $shiftRepo->delete($event->shiftId);
    }

    public function onShiftUntaken(ShiftUntaken $event)
    {
        $account = Account::find($event->accountId);
        $shiftRepo = new ShiftRepository($account);
        $data = [];
        $data['status'] = Shift::STATUS_OPEN;
        $data['worker_id'] = null;
        $shiftRepo->update($data, $event->shiftId);
    }

    public function onShiftTaken(ShiftTaken $event)
    {
        $account = Account::find($event->accountId);
        $shiftRepo = new ShiftRepository($account);
        $data = [];
        $data['status'] = Shift::STATUS_TAKEN;
        $data['worker_id'] = $event->workerId;
        $shiftRepo->update($data, $event->shiftId);
    }

    public function onShiftCancelled(ShiftCancelled $event)
    {
        $account = Account::find($event->accountId);
        $shiftRepo = new ShiftRepository($account);
        $data = [];
        $data['status'] = Shift::STATUS_CANCELLED;
        $shiftRepo->update($data, $event->shiftId);
    }
}
