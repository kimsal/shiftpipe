<?php

namespace App\Projectors;

use App\Repositories\AccountRepository;
use App\Repositories\WorkerRepository;
use App\StorableEvents\WorkerCreated;
use App\StorableEvents\WorkerUpdated;
use Spatie\EventSourcing\Projectors\Projector;
use Spatie\EventSourcing\Projectors\ProjectsEvents;

final class WorkerProjector implements Projector
{
    use ProjectsEvents;

    public function onWorkerCreated(WorkerCreated $event)
    {
        $account = (new AccountRepository())->get($event->accountId);
        $repo = new WorkerRepository($account);
        $data = [];
        $data['account_id'] = $event->accountId;
        $data['email'] = $event->email;
        $data['name'] = $event->name;
        $data['phone'] = $event->phone;
        $data['status'] = $event->status;
        $id = $event->workerId;
        $data['id'] = (string)$id;
        $info = $repo->create($data);
    }

    public function onWorkerUpdated(WorkerUpdated $event)
    {
        $account = (new AccountRepository())->get($event->accountId);
        $repo = new WorkerRepository($account);
        $repo->update($event->workerData, $event->workerId);
    }
}
