<?php

namespace App\Projectors;

use App\Repositories\AccountRepository;
use App\Repositories\UserRepository;
use App\StorableEvents\UserCreated;
use Spatie\EventSourcing\Projectors\Projector;
use Spatie\EventSourcing\Projectors\ProjectsEvents;

final class UserProjector implements Projector
{
    use ProjectsEvents;

    public function onUserCreated(UserCreated $event)
    {
        $account = (new AccountRepository())->get($event->accountId);
        $user = (new UserRepository($account))->create(['id'=>$event->id, 'account_id'=>$event->accountId, 'email'=>$event->email, 'name'=>$event->name, 'hashedPassword'=>$event->hashedPassword]);
    }
}
