<?php

namespace App\Projectors;

use App\Repositories\AddressRepository;
use App\Repositories\UserRepository;
use App\StorableEvents\AddressCreated;
use App\StorableEvents\AddressUpdated;
use Spatie\EventSourcing\Projectors\Projector;
use Spatie\EventSourcing\Projectors\ProjectsEvents;
use Spatie\EventSourcing\StoredEvent;

final class AddressProjector implements Projector
{
    use ProjectsEvents;


    /**
     * note for later - you can get the stored event object itself
     */
//    public function onAddressCreated(AddressCreated $event, StoredEvent $storedEvent) { }

    public function onAddressCreated(AddressCreated $event)
    {
        (new AddressRepository())->create((array)$event);
    }

    public function onAddressUpdated(AddressUpdated $event)
    {
        (new AddressRepository())->update((array)$event, $event->id);
    }
}
