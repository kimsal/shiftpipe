<?php

namespace App\Projectors;

use App\Repositories\AccountRepository;
use App\Repositories\UserRepository;
use App\StorableEvents\AccountCreated;
use Spatie\EventSourcing\Projectors\Projector;
use Spatie\EventSourcing\Projectors\ProjectsEvents;

final class AccountProjector implements Projector
{
    use ProjectsEvents;

    public function onAccountCreated(AccountCreated $event)
    {
        $account = (new AccountRepository())->create(['id'=>$event->accountId, 'name'=>$event->accountName]);
        $user = (new UserRepository($account))->create(['id'=>$event->userId, 'email'=>$event->email, 'name'=>$event->name, 'hashedPassword'=>$event->hashedPassword]);
    }
}
