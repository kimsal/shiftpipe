<?php

namespace App\Http\Controllers;


class FrontController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Show the front home page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('front');
    }
}
