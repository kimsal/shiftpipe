<?php

namespace App\Http\Controllers\Api;

use App\Models\Location;
use App\Models\User;
use App\Repositories\AccountRepository;
use App\Repositories\LocationRepository;
use App\Repositories\UserRepository;
use Geocodio\Geocodio;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class AddressController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function verify(Request $request)
    {
        $_address = $request->json('address');
        $address = "";
        $address = $_address['address1'].", ";
        $address .= $_address['city'].", ";
        $address .= $_address['state']." ";
        $address .= $_address['postal_code'];
        $geocoder = new Geocodio();

        $geocoder->setApiKey('51e3f2e2aa014b552b6a0be16fe9ee40699f442');

        $response = $geocoder->geocode($address);
        $return = [];
        foreach($response->results as $result)
        {
            if($result->accuracy_type == 'rooftop')
            {
                $return[] = $result;
            }
        }
        return response()->json(['orig'=>$address, 'addresses'=>$return]);
    }

}
