<?php

namespace App\Http\Controllers\Api;

use App\Repositories\UserRepository;
use App\Services\AccountService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class AccountController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function create(Request $request)
    {
        $accountService = new AccountService();

        $user = $accountService->createAccountWithOwnerUser($request->get('email'),
            $request->get('name'),
            $request->get('password'),
            $request->get('accountName'));

        return response()->json(['user'=>$user, 'account'=>$user->account]);
    }
}
