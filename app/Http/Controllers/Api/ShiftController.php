<?php

namespace App\Http\Controllers\Api;

use App\Models\Shift;
use App\Models\User;
use App\Repositories\AccountRepository;
use App\Repositories\LocationRepository;
use App\Repositories\ShiftRepository;
use App\Repositories\UserRepository;
use App\Services\ShiftService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class ShiftController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function create(Request $request)
    {
        // need location object
        /** @var User $requestUser */
        $requestUser = Auth::user();

        $location = (new LocationRepository($requestUser->account))->get($request->get('location_id'));
        $shiftService = new ShiftService($requestUser->account);
        $wallStartDate = $request->get('wallstart_date');
        $wallStartTime = $request->get('wallstart_time');
        $wallEndDate = $request->get('wallend_date');
        $wallEndTime = $request->get('wallend_time');
        $shift = $shiftService->schedule($location->id, $wallStartDate, $wallStartTime, $wallEndDate, $wallEndTime, $requestUser);

        return response()->json(['shift' => $shift]);
    }

    public function createMass(Request $request)
    {
        // need location object
        /** @var User $requestUser */
        $requestUser = Auth::user();

        $massShifts = $request->get('shifts');
        $shifts = [];
        foreach($massShifts as $shift) {
            $location = (new LocationRepository($requestUser->account))->get($shift['location_id']);
            $shiftService = new ShiftService($requestUser->account);
            $wallStartDate = $shift['wallstart_date'];
            $wallStartTime = $shift['wallstart_time'];
            $wallEndDate = $shift['wallend_date'];
            $wallEndTime = $shift['wallend_time'];
            $shifts[] = $shiftService->schedule($location->id, $wallStartDate, $wallStartTime, $wallEndDate, $wallEndTime, $requestUser);
        }
        return response()->json(['shifts' => $shifts]);
    }

    public function update(Request $request)
    {
        // need location object
        /** @var User $requestUser */
        $requestUser = Auth::user();

        $shift = (new ShiftRepository($requestUser->account))->get($request->get('id'));
        $locationId = $request->get('location_id') ?? $shift->location_id;
        $location = (new LocationRepository($requestUser->account))->get($locationId);
        $shiftService = new ShiftService($requestUser->account);
        $wallStartDate = $request->get('wallstart_date');
        $wallStartTime = $request->get('wallstart_time');
        $wallEndDate = $request->get('wallend_date');
        $wallEndTime = $request->get('wallend_time');
        $shiftId = $request->get('id');
        $shift = $shiftService->update($shiftId, $location->id, $wallStartDate, $wallStartTime, $wallEndDate, $wallEndTime, $requestUser);
        return response()->json(['shift' => $shift]);
    }

    public function delete(Request $request, Shift $shift)
    {
        // need location object
        /** @var User $requestUser */
        $requestUser = Auth::user();

//        $shift = (new ShiftRepository($requestUser->account))->get($request->get('id'));
        $shiftService = new ShiftService($requestUser->account);
        $shiftService->delete($shift->id, $requestUser);
        return response()->json(['status' => 'OK']);
    }

    public function all(Request $request)
    {
        /** @var User $requestUser */
        $requestUser = Auth::user();
        $shiftRepo = new ShiftRepository($requestUser->account);
        $shifts = $shiftRepo->allWithLocations();
        return response()->json(['shifts' => $shifts]);
    }

    public function get(Request $request, Shift $shift)
    {
        /** @var User $requestUser */
        $requestUser = Auth::user();
        if ($shift->location->account->id != $requestUser->account->id) {
            throw new \Exception("not your account");
        }
        return response()->json(['shift' => $shift]);
    }

    public function take(Request $request)
    {
        /** @var User $requestUser */
        $requestUser = Auth::user();
        $shiftService = new ShiftService($requestUser->account);
        $data = $request->all();
        $shiftService->take($data['shiftId'] ?? $data[0]['shiftId'], $data['workerId'], $requestUser->id);
        return response()->json(['status' => 'OK']);
    }

    public function untake(Request $request)
    {
        /** @var User $requestUser */
        $requestUser = Auth::user();
        $shiftService = new ShiftService($requestUser->account);
        $data = $request->all();
        $shiftService->untake($data['shiftId'] ?? $data[0]['shiftId'], $data['workerId'], $requestUser->id);
        return response()->json(['status' => 'OK']);
    }
}
