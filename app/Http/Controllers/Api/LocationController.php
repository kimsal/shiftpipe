<?php

namespace App\Http\Controllers\Api;

use App\Models\Location;
use App\Models\User;
use App\Repositories\AccountRepository;
use App\Repositories\AddressRepository;
use App\Repositories\LocationRepository;
use App\Repositories\UserRepository;
use App\Services\AddressService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class LocationController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function create(Request $request)
    {
        /** @var User $requestUser */
        $requestUser = Auth::user();
        $locRepo = new LocationRepository($requestUser->account);
        $location = $locRepo->create($request->all());
        return response()->json(['location'=>$location]);
    }

    public function get(Request $request, Location $location)
    {
        /** @var User $requestUser */
        $requestUser = Auth::user();
        if($location->account->id != $requestUser->account->id)
        {
            throw new \Exception("Not your account");
        }
        return response()->json(['location'=>$location]);
    }

    public function update(Request $request)
    {
        /** @var User $requestUser */
        $requestUser = Auth::user();

        $locRepo = new LocationRepository($requestUser->account);
        $locationInfo = $request->get('location');

        $add = $request->json('address', null);
        if ($add != null) {
            if (@$add['id'] != null) {
                AddressService::updateByArray($add);
                $address = (new AddressRepository())->get($add['id']);
            } else {
                $address = AddressService::createByArray($add);
            }
            $locationInfo['address_id'] = $address->id;
        }

        // we may get the 'address' relation key sent back as well.
        if(array_key_exists("address", $locationInfo)) {
            unset($locationInfo['address']);
        }

        $location = $locRepo->update($locationInfo, $locationInfo['id']);
        return response()->json(['location'=>$location]);
    }

    public function delete(Request $request, Location $location)
    {
        /** @var User $requestUser */
        $requestUser = Auth::user();
        $locRepo = new LocationRepository($requestUser->account);
        $location = $locRepo->delete($location->id);
        return response()->json(['status'=>'OK']);
    }

    public function all(Request $request)
    {
        /** @var User $requestUser */
        $requestUser = Auth::user();
        $locRepo = new LocationRepository($requestUser->account);
        $locations = $locRepo->all();
        return response()->json(['locations'=>$locations]);
    }
}
