<?php

namespace App\Http\Controllers\Api;

use App\Models\Invitation;
use App\Models\Location;
use App\Models\User;
use App\Models\Worker;
use App\Repositories\AccountRepository;
use App\Repositories\AddressRepository;
use App\Repositories\InvitationRepository;
use App\Repositories\LocationRepository;
use App\Repositories\UserRepository;
use App\Repositories\WorkerRepository;
use App\Services\AddressService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class WorkerController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function create(Request $request)
    {
        /** @var User $requestUser */
        $requestUser = Auth::user();
        $repo = new WorkerRepository($requestUser->account);
        $item = $repo->createAndInvite($request->all());
        return response()->json(['worker' => $item]);
    }

    public function get(Request $request, Worker $worker)
    {
        /** @var User $requestUser */
        $requestUser = Auth::user();
        if ($worker->account->id != $requestUser->account->id) {
            throw new \Exception("Not your account");
        }
        return response()->json(['worker' => $worker]);
    }

    public function update(Request $request)
    {
        /** @var User $requestUser */
        $requestUser = Auth::user();

        $repo = new WorkerRepository($requestUser->account);
        $worker = $request->json('worker');
        $item = $repo->update($worker, $worker['id']);

        return response()->json(['worker' => $item]);
    }

    public function updateAddress(Request $request)
    {
        /** @var User $requestUser */
        $requestUser = Auth::user();

        $add = $request->json('address');
        if(@$add['id']!=null)
        {
            AddressService::updateByArray($add);
            $address = (new AddressRepository())->get($add['id']);
        } else {
            $address = AddressService::createByArray($add);
        }

        $repo = new WorkerRepository($requestUser->account);
        $worker = $request->json('worker');
        $worker['address_id'] = $address->id;
        $item = $repo->update($worker, $worker['id']);

        return response()->json(['worker' => $item]);
    }

    public function delete(Request $request, Worker $worker)
    {
        /** @var User $requestUser */
        $requestUser = Auth::user();
        $repo = new WorkerRepository($requestUser->account);
        $item = $repo->delete($worker->id);
        return response()->json(['status' => 'OK']);
    }

    public function all(Request $request)
    {
        /** @var User $requestUser */
        $requestUser = Auth::user();
        $repo = new WorkerRepository($requestUser->account);
        $workers = $repo->all();
        return response()->json(['workers' => $workers]);
    }

    /**
     * need to not have any auth around this... ?
     *
     * @param Invitation $invitation
     * @return \Illuminate\Http\JsonResponse
     */
    public function invitation(Request $request, Invitation $invitation)
    {
        /** @var User $requestUser */
        $repo = new InvitationRepository($invitation->worker->account);
        $repo->accept($invitation, $request->toArray()['worker']);
        return response()->json(['status' => 'OK']);
    }

    public function me(Request $request)
    {
        /** @var User $requestUser */
        $requestUser = Auth::user();
        $requestUser->load(['worker','worker.address']);

        return response()->json(['me' => $requestUser]);
    }
}
