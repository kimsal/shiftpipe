<?php

namespace App;

use App\Traits\UsesUlid;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends \App\Models\User
{
    public $incrementing = false;

    protected $keyType = "string";
}
