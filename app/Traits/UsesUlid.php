<?php


namespace App\Traits;

use Ulid\Ulid;

trait UsesUlid
{

    protected static function bootUsesUlid()
    {
        static::creating(function ($model) {
            if (! $model->getKey()) {
                $model->{$model->getKeyName()} = (string) Ulid::generate();
            }
        });
    }

}
