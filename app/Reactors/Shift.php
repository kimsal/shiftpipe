<?php

namespace App\Reactors;

use Spatie\EventSourcing\EventHandlers\EventHandler;
use Spatie\EventSourcing\EventHandlers\HandlesEvents;

final class Shift implements EventHandler
{
    use HandlesEvents;

    public function onEventHappened(EventHappened $event)
    {
    }
}
