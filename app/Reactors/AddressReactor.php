<?php

namespace App\Reactors;

use App\StorableEvents\AddressCreated;
use Spatie\EventSourcing\EventHandlers\EventHandler;
use Spatie\EventSourcing\EventHandlers\HandlesEvents;

final class AddressReactor implements EventHandler
{
    use HandlesEvents;

    public function onAddressCreated(AddressCreated $event)
    {
    }
}
