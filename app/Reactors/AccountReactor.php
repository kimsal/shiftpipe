<?php

namespace App\Reactors;

use App\StorableEvents\AccountCreated;
use Spatie\EventSourcing\EventHandlers\EventHandler;
use Spatie\EventSourcing\EventHandlers\HandlesEvents;

final class AccountReactor implements EventHandler
{
    use HandlesEvents;

    public function onAccountCreated(AccountCreated $event)
    {
        // need emails sent on account creation?
        // we'll do them in the reactor, which only gets run on the first event
        // replays will only run the projects, not the reactor
    }
}
