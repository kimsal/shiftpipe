<?php

namespace App\Reactors;

use App\Mail\WorkerInvite;
use App\Repositories\AccountRepository;
use App\Repositories\InvitationRepository;
use App\StorableEvents\InvitationCreated;
use Illuminate\Support\Facades\Mail;
use Spatie\EventSourcing\EventHandlers\EventHandler;
use Spatie\EventSourcing\EventHandlers\HandlesEvents;

final class InvitationReactor implements EventHandler
{
    use HandlesEvents;

    // reactors will only get called on initial run, not on event sourcing replays
    public function onInvitationCreated(InvitationCreated $event)
    {
        $account = (new AccountRepository())->get($event->accountId);
        $invitation = (new InvitationRepository($account))->get($event->inviteId);
        Mail::to($invitation->worker->email)->queue(new WorkerInvite($invitation));
    }
}
