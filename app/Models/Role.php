<?php


namespace App\Models;


class Role extends \Spatie\Permission\Models\Role
{

    const ROLE_SUPERUSER = 'superuser';
    const ROLE_ACCOUNT_OWNER = 'accountowner';
    const ROLE_LOCATION_ADMIN = 'locationadmin';
    const ROLE_LOCATION_USER = 'locationuser';
    const ROLE_WORKER = 'worker';

    const ROLE_LIST = [self::ROLE_SUPERUSER, self::ROLE_ACCOUNT_OWNER, self::ROLE_LOCATION_ADMIN,
        self::ROLE_LOCATION_USER, self::ROLE_WORKER];

}
