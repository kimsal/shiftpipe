<?php

namespace App\Models;

use App\Traits\UsesUlid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends UlidModel
{
    use SoftDeletes;

    public $table = "account";

    protected $guarded = [''];

    public function users()
    {
        return $this->hasMany(\App\User::class);
    }
}
