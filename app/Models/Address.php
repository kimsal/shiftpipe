<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends UlidModel
{
    use SoftDeletes;

    public $table = "address";

    protected $fillable = ['id','address1','address2','city','state','postal_code','notes','longitude','latitude'];

}
