<?php

namespace App\Models;

use App\Traits\UsesUlid;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Worker extends UlidModel
{

    const STATUS_PENDING = "pending";
    const STATUS_INVITED = "invited";
    const STATUS_ACTIVE = "active";
    const STATUS_SUSPENDED = "suspended";

    public $table = "worker";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone', 'account_id', 'id', 'user_id', 'status', 'address_id'
    ];

    public function account() {
        return $this->belongsTo(Account::class);
    }

    public function user() {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function address() {
        return $this->belongsTo(\App\Models\Address::class);
    }

    public function invitations()
    {
        return $this->hasMany(Invitation::class);
    }
}
