<?php

namespace App\Models;

use App\Traits\UsesUlid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Ulid\Ulid;

class Location extends UlidModel
{
    use SoftDeletes;

    public $table = "location";

    protected $guarded = [''];

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }
}
