<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Distance extends Model
{

    public $table = "distance";

    protected $guarded = ['id'];

    public function targetAddress()
    {
        return $this->hasOne(Address::class, 'id', 'address_to_id');
    }
}
