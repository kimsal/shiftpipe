<?php

namespace App\Models;

use App\Traits\UsesUlid;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class Invitation extends UlidModel
{

    const STATUS_CREATED = "created";
    const STATUS_SENT = "sent";
    const STATUS_CLICKED = "clicked";
    const STATUS_ACTIVE = "active";
    const STATUS_SUSPENDED = "suspended";

    public $table = "invitation";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_id', 'user_id', 'status', 'worker_id', 'email', 'code', 'id'
    ];

    public function worker() {
        return $this->belongsTo(Worker::class);
    }

}
