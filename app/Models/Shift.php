<?php

namespace App\Models;

use App\Traits\UsesUlid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shift extends UlidModel
{
    use SoftDeletes;

    const STATUS_OPEN = "open";
    const STATUS_RESERVED = "reserved";
    const STATUS_TAKEN = "taken";
    const STATUS_DONE = "done";
    const STATUS_CANCELLED = "cancelled";
    const STATUS_DELETED = "deleted";

    public $table = "shift";

    protected $guarded = [''];

    public function poster()
    {
        return $this->belongsTo(\App\User::class, "poster_id", "id");
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }

}
