<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends UlidModel
{
    use SoftDeletes;

    public $table = "profile";

    protected $guarded = [''];

}
