<?php

namespace App\Models;


use App\Traits\UsesUlid;
use Illuminate\Database\Eloquent\Model;

class UlidModel extends Model
{
    use UsesUlid;

    public $incrementing = false;

    protected $keyType = "string";

}
