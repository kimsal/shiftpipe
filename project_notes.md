# notes

Creating an api-driven location/shift management system for work scheduling and notification

Will use some common tools as base - may add more here.

Event-sourcing eventually, but for right now, will just add permissioning package

permission system mentions it, and it's a near requirement for event sourcing.

Using https://docs.spatie.be/laravel-permission/v3/introduction/
Set up with ULID package from robinvdvleuten/ulid

User model in Models folder is main - \App\User subclasses for now.  Wanted 
everything in Models and below, but some core l6 stuff seems to assume \App\User.

UserRepository handles password hashing of user - main register does its own 
via direct interaction with User model.

short term testing, we'll have a location not require an account, so we 
can test some shift stuff first

need to combine the walldate/walltime for comparison
but there's not a corresponding mysql field type (IIRC)
runtime combining will be slow...  hrmm....
may have misunderstood - 'datetime' may be acceptable... will check...


# model needs

##user
user of the system

##role
what is this user (admin? scheduled user? shift manager? etc)

##group
group of people (functional group?  geographic group?)

##permission
things a user can do

##account
the main customer entity

##location
places a user can perform a shift/activity

##address
city/state/etc

##shift
defined time and location for activity to take place
time/date/skill


##shiftrequest
indication that user wants to perform at a shift

may allow for queueing and 'wait list' - user A cancels, user B was next indicating interest, etc

may be optional if people can claim shift directly (or as assigned directly?)

##skill
skill/certification desired for particular activity/shift, and skill/certifications users posess

will need user_skill and shift_skill relation tables

##feedback
user feedback on shift (from scheduled user's perspective)
user feedback on scheduled user (from account/location/manager perspective)

##profile
details about a user - public image, contact info - private info (functional preferences, etc)


# functional needs
in no particular order...

## user register
* user can register and set up "account" for hosting work locations and shift work (paying customer?)
* user can register as someone interested in taking work

## manage work locations
* account users can manage work locations (add/edit/delete)
* from work locations, invite users to be associated with specific locations
* association will have explicit role or permission

## create shift
* account user can create shift for location
* shift will be associated with specific date and time in future
* shift will be associated with location
* shift will be associated with 0 or more defined skills
* shift will be 'live' (visible to users searching) upon saving
* shift may have statuses (open, taken, cancelled, fulfilled, deleted)

## delete or cancel shift
* mark shift as deleted or cancelled
* notify users related to shift?
* deleted would not be visible in searches ever
* cancelled may show up with status 'cancelled'
* require reason for cancellation?  display reason?

## take shift
* shift worker users can indicate interest in a shift
* first pass - we will just allow them to 'take' a shift
* shift marked as taken - not able to be taken by others
* user can 'untake' shift based on ... time deadline (24 hours before?)
* (will skip untake time limitations for now)
* can not untake after shift is fulfilled

## notifications
* shift worker users will receive notifications about shift changes/cancellations/deletions
* location admin users will receive notifications about shift changes
* location admin users will receive notifications about taken shifts

## account users
* account users will have account slug ("Jackson Concrete" may choose "jcon" to use in URLs)
* url slugs will be used to implement multi-tenancy
* account users can modify account-level preferences for site-wide functionality like messaging
(banner messages on site, system-wide emails, etc)



# API test steps
1. create a super user (user with superuser role)
2. as SU create account with owner role
3. as the newly created owner, create location
4. as new owner, create second location
5. as new owner, get list of locations - confirm 2 


# sample front end needs
So.. this was some more functional thought on what needs to be supported, and 
there's still likely some internals and API work that should be done.

1. Login as superuser
    1.  view list of account/users
    1.  log in as an account owner
1. register new 'account' (customer) with username/email as primary account holder
    1. this is a tenant - they'll need a slug
    1. slug will be used for foo.com/accountslug (or accountslug.foo.com)
1. register new user - shift worker
    1. shift workers will only be registering at a particular tenant
    1. a shift worker might end up being associated with multiple account/tenants, and
    would need to 'switch account' or similar to jump between that info
1. login as account owner
1. as account owner
    1. view locations
    1. create location
    1. edit location
    1. delete location
1. as account owner
    1. view shifts
    1. create shift
    1. view shifts waiting for approval
    1. approve shift requests
    1. edit/update shift
    1. cancel shift
    1. delete shift
1. as shift worker
    1. login 
    1. view locations in geo area for the account I'm connected with
    1. view shifts for a location
    1. view shifts for multiple locations in particular geo
    1. 'request' the shift
        1. shift may be auto approved, or be queued for manual approval
        1. receive notification (email/sms/etc) when requested, approved, changed, denied, cancelled
        
# notifications
use laravel notification for indicating shift request/taken/deleted/cancelled    
    

    
re: 2.ii - this may be a problem for people seeing consolidated calendar views?
may not be a practical problem...?

# auth

OOPS - forgot to include api_token for user table, and to include in app.blade base



#dec 22 
## workers
we'll have a worker table tied to a user, and tied to an account.

shift info will be tied to the worker profile table vs the user table directly.

So... to 'invite' a worker, we'll send an email
when the worker is first added, and we'll allow a 're-invite' 
until the related user is already connected.

when the user clicks an invite link, and adds a password, we'll 
connect the created user account to the worker profile, and 
set worker status to 'active'.  

Statuses - 'invited', 'clicked' (if they click the invite), 'active'
and 'suspended'.... (can change later).

For second worker profile for different account... (top level account)...
similar process, but we can have them authenticate and show 
they're connecting to a second account.



# bootswatch

we'll add bootswatch to give us a few different themes to choose from...

edit app.scss and change the theme name to one of the bootswatch theme names


# big chore - app\user

when working on model/permission/role, the base
class is recorded, and there was some confustion between 
app\user and app\models\user

app\user is the default, and some core laravel stuff deals with it.
the app\user is (now) just a subclass of the app\models\user, but
some things were getting amu directly, and some getting laravel-provided
app\user directly, creating confusion.  quick commit to 
push everything to use app\user...

   

# geocoder
https://github.com/soal/vue-mapbox-geocoder/pull/14/files

install and docs here: https://github.com/soal/vue-mapbox-geocoder

but need the PR from above patched manually?


# jan 9 2020
cleaning up address verify - using geocodio
mapbox was... overkill... 
UI cleanup now

Needs
* add address to locations
