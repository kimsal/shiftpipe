<?php

namespace Tests\Feature;

use App\Models\Role;
use App\Repositories\AccountRepository;
use App\Repositories\LocationRepository;
use App\Repositories\SuperUserRepository;
use App\Repositories\UserRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class UserAccountRegisterTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @return void
     */
    public function testAccountCreation()
    {
        $email = 'user' . time() . '@example.com';
        $accountName = 'Sample Account 1';
        $response = $this->post("/register", [
            'account_name' => $accountName,
            'email' => 'user' . time() . '@example.com',
            'password' => 'password',
            'password_confirmation' => 'password',
            'name' => 'Test user'
        ]);
        $response->assertStatus(302);
        $response->assertRedirect("/home");

        // may be all we can do for this first test
        $repo = new AccountRepository();
        $account = $repo->firstWhere("name", $accountName);
        $this->assertCount(1, $account->users);
    }

    public function testAccountAndLocationCreation()
    {
        $email = 'user' . time() . '@example.com';
        $accountName = 'Sample Account 1';
        $response = $this->post("/register", [
            'account_name' => $accountName,
            'email' => 'user' . time() . '@example.com',
            'password' => 'password',
            'password_confirmation' => 'password',
            'name' => 'Test user'
        ]);
        $response->assertStatus(302);
        $response->assertRedirect("/home");

        // may be all we can do for this first test
        $repo = new AccountRepository();
        $account = $repo->firstWhere("name", $accountName);
        $this->assertCount(1, $account->users);
        $user = (new UserRepository($account))->get($account->users[0]->id);

        // we should have 0 locations
        $response = $this->actingAs($user, 'api')->get("/api/location/all");
        $response->assertStatus(200);
        $data = json_decode($response->getContent());
        $this->assertCount(0, $data->locations);

        // let's add one location
        $response = $this->actingAs($user, 'api')->put("/api/location/create", [
            'name'=>"First location"
        ]);
        $response->assertStatus(200);

        // we should have 1 location
        $response = $this->actingAs($user, 'api')->get("/api/location/all");
        $response->assertStatus(200);
        $data = json_decode($response->getContent());
        $this->assertCount(1, $data->locations);

    }

}
