<?php

namespace Tests\Feature;

use App\Models\Address;
use App\Services\DirectionService;
use App\Services\DistanceService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class DistanceTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @param array $start
     * @param array $targets
     * @param float $meters
     * @param int $expectedMeters
     * @param float $miles
     * @param int $expectedMiles
     * @param float $seconds
     * @param int $expectedSeconds
     * @return void
     * @dataProvider addressData
     */
    public function testDistance1(array $start, array $targets,
        float $meters, int $expectedMeters,
        float $miles, int $expectedMiles,
        float $seconds, int $expectedSeconds)
    {
        $from = new Address();
        $from->latitude = $start[0];
        $from->longitude = $start[1];
        $from->save();

        $service = new DistanceService();

        foreach ($targets as $target) {
            $targetAddress = new Address();
            $targetAddress->latitude = $target[0];
            $targetAddress->longitude = $target[1];
            $targetAddress->save();
            $service->addDistance($from, $targetAddress);
        }

        $found = $service->findAddressesByMeters($from, $meters);
        $this->assertCount($expectedMeters, $found);
        $found = $service->findAddressesByMiles($from, $miles);
        $this->assertCount($expectedMiles, $found);
        $found = $service->findAddressesBySeconds($from, $seconds);
        $this->assertCount($expectedSeconds, $found);
    }

    public function addressData()
    {
        return [
            [
                'start' => [-73.989, 40.733],
                'targets' => [
                    [-74, 40.733],
                    [-74.02, 40.733],
                    [-74.04, 40.733],
                    [-74.06, 40.733],
                ],
                'meters' => 4000,
                'expectedMeters' => 2,
                'miles' => 0,
                'expectedMiles' => 0,
                'seconds' => 600, // 10 minutes
                'expectedSeconds' => 1,
            ],
            [
                'start' => [-73.989, 40.733],
                'targets' => [
                    [-74, 40.733],
                    [-74.02, 40.733],
                    [-74.04, 40.733],
                ],
                'meters' => 6000,
                'expectedMeters' => 3,
                'miles' => 30,
                'expectedMiles' => 3,
                'seconds' => 1200, // 20 minutes
                'expectedSeconds' => 3,
            ],
            [
                'start' => [-78.4320184, 36.0181817],
                'targets' => [
                    [-74, 40.733],
                    [-74.02, 40.733],
                    [-74.04, 40.733],
                ],
                'meters' => 6000,
                'expectedMeters' => 0,
                'miles' => 6000,
                'expectedMiles' => 3,
                'seconds' => 36000, // 10 hours - should normally be around 8.2 hours between the locations above
                'expectedSeconds' => 3,
            ],
        ];
    }

}
