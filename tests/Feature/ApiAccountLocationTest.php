<?php

namespace Tests\Feature;

use App\Models\Role;
use App\Repositories\AccountRepository;
use App\Repositories\LocationRepository;
use App\Repositories\SuperUserRepository;
use App\Repositories\UserRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class ApiAccountLocationTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @return void
     */
    public function testApiAccountCreation()
    {
        $repo = new SuperUserRepository();
        $user = $repo->createWithRole(['email' => 'admin@admin.com', 'name' => 'foo', 'password' => 'super'], Role::ROLE_SUPERUSER);
        $response = $this->actingAs($user, 'api')->put("/api/account/create", [
            'name' => 'account manager', 'email' => 'man@example.com', 'password' => 'password', 'accountName' => 'Account ABC'
        ]);
        $response->assertStatus(200);
        $data = json_decode($response->getContent());
        $this->assertEquals("man@example.com", $data->user->email);
        $this->assertEquals("Account ABC", $data->account->name);
    }

    /**
     * @return void
     */
    public function testApiAccountCreateLocation()
    {
        $repo = new SuperUserRepository();
        $user = $repo->createWithRole(['email' => 'admin@admin.com', 'name' => 'foo', 'password' => 'super'], Role::ROLE_SUPERUSER);
        $response = $this->actingAs($user, 'api')->put("/api/account/create", [
            'name' => 'account manager', 'email' => 'man@example.com', 'password' => 'password', 'accountName' => 'Account ABC'
        ]);
        $response->assertStatus(200);
        $data = json_decode($response->getContent());
        $account = (new AccountRepository())->get($data->account->id);
        $accountOwner = (new UserRepository($account))->get($data->user->id);
        session()->flush();
        $response = $this->actingAs($accountOwner, 'api')->put("/api/location/create", [
            'name' => 'sample location'
        ]);
        $response->assertStatus(200);
        $data = json_decode($response->getContent());
        $location = (new LocationRepository($accountOwner->account))->get($data->location->id);
        $this->assertEquals("sample location", $location->name);

        $response = $this->actingAs($accountOwner, 'api')->get("/api/location/all");
        $response->assertStatus(200);
        $data = json_decode($response->getContent());
        $this->assertCount(1, $data->locations);


        $response = $this->actingAs($accountOwner, 'api')->put("/api/location/create", [
            'name' => 'sample location 2'
        ]);
        $response->assertStatus(200);
        $data = json_decode($response->getContent());
        $location = (new LocationRepository($accountOwner->account))->get($data->location->id);
        $this->assertEquals("sample location 2", $location->name);

        $response = $this->actingAs($accountOwner, 'api')->get("/api/location/all");
        $response->assertStatus(200);
        $data = json_decode($response->getContent());
        $this->assertCount(2, $data->locations);
    }


    /**
     * @return void
     */
    public function testApiAccountTestUpdate()
    {
        $repo = new SuperUserRepository();
        $user = $repo->createWithRole(['email' => 'admin@admin.com', 'name' => 'foo', 'password' => 'super'], Role::ROLE_SUPERUSER);
        $response = $this->actingAs($user, 'api')->put("/api/account/create", [
            'name' => 'account manager', 'email' => 'man@example.com', 'password' => 'password', 'accountName' => 'Account ABC'
        ]);
        $response->assertStatus(200);
        $data = json_decode($response->getContent());
        $account = (new AccountRepository())->get($data->account->id);
        $accountOwner = (new UserRepository($account))->get($data->user->id);
        session()->flush();
        $response = $this->actingAs($accountOwner, 'api')->put("/api/location/create", [
            'name' => 'sample location'
        ]);
        $response->assertStatus(200);
        $data = json_decode($response->getContent());
        $location = (new LocationRepository($accountOwner->account))->get($data->location->id);
        $this->assertEquals("sample location", $location->name);

        $response = $this->actingAs($accountOwner, 'api')->get("/api/location/all");
        $response->assertStatus(200);
        $data = json_decode($response->getContent());
        $this->assertCount(1, $data->locations);


        $locationToUpdate = $data->locations[0];
        $response = $this->actingAs($accountOwner, 'api')
            ->post("/api/location/update", [
                'location' => ['name' => 'sample location 5', 'id' => $locationToUpdate->id]
            ]);
        $response->assertStatus(200);
        $data = json_decode($response->getContent());

        // get the updated location
        $response = $this->actingAs($accountOwner, 'api')->get("/api/location/" . $data->location->id);
        $response->assertStatus(200);
        $data = json_decode($response->getContent());

        $this->assertEquals("sample location 5", $data->location->name);
        $this->assertEquals($locationToUpdate->id, $data->location->id);

        $response = $this->actingAs($accountOwner, 'api')->get("/api/location/all");
        $response->assertStatus(200);
        $data = json_decode($response->getContent());
        $this->assertCount(1, $data->locations);
    }

    /**
     * @return void
     */
    public function testApiAccountTestDelete()
    {
        $repo = new SuperUserRepository();
        $user = $repo->createWithRole(['email' => 'admin@admin.com', 'name' => 'foo', 'password' => 'super'], Role::ROLE_SUPERUSER);
        $response = $this->actingAs($user, 'api')->put("/api/account/create", [
            'name' => 'account manager', 'email' => 'man@example.com', 'password' => 'password', 'accountName' => 'Account ABC'
        ]);
        $response->assertStatus(200);
        $data = json_decode($response->getContent());
        $account = (new AccountRepository())->get($data->account->id);
        $accountOwner = (new UserRepository($account))->get($data->user->id);
        session()->flush();
        $response = $this->actingAs($accountOwner, 'api')->put("/api/location/create", [
            'name' => 'sample location'
        ]);
        $response->assertStatus(200);
        $data = json_decode($response->getContent());
        $location = (new LocationRepository($accountOwner->account))->get($data->location->id);
        $this->assertEquals("sample location", $location->name);

        $response = $this->actingAs($accountOwner, 'api')->get("/api/location/all");
        $response->assertStatus(200);
        $data = json_decode($response->getContent());
        $this->assertCount(1, $data->locations);

        $locationToUpdate = $data->locations[0];
        $response = $this->actingAs($accountOwner, 'api')->delete("/api/location/delete/" . $locationToUpdate->id);
        $response->assertStatus(200);
        $data = json_decode($response->getContent());

        // get the updated location
        $response = $this->actingAs($accountOwner, 'api')->get("/api/location/" . $locationToUpdate->id);
        $response->assertStatus(404);

        $response = $this->actingAs($accountOwner, 'api')->get("/api/location/all");
        $response->assertStatus(200);
        $data = json_decode($response->getContent());
        $this->assertCount(0, $data->locations);
    }
}
