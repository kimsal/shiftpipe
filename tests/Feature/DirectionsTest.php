<?php

namespace Tests\Feature;

use App\Models\Address;
use App\Services\DirectionService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class DirectionsTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @return void
     */
    public function testShortDirections()
    {
        // two blocks in NYC
        // -73.989,40.733;-74,40.733
        $x = new DirectionService();
        $l1 = new Address();
        $l2 = new Address();
        $l1->latitude = -73.989;
        $l1->longitude = 40.733;
        $l2->latitude = -74;
        $l2->longitude = 40.733;
        $output = $x->getDirections($l1, $l2);
        $this->assertArrayHasKey('time', $output);
        $this->assertArrayHasKey('distance', $output);

        $this->assertStringContainsString('minutes', $output['time']['string']);
        $this->assertStringContainsString('miles', $output['distance']['string']);
    }

    public function testLongDirections()
    {
        // MI to NC
        // -82.964685, 42.5946354
        // -78.4320184, 36.0181817
        $x = new DirectionService();
        $l1 = new Address();
        $l2 = new Address();
        $l1->latitude = -82.964685;
        $l1->longitude = 42.5946354;
        $l2->latitude = -78.4320184;
        $l2->longitude = 36.0181817;
        $output = $x->getDirections($l1, $l2);
        $this->assertArrayHasKey('time', $output);
        $this->assertArrayHasKey('distance', $output);

        $this->assertStringContainsString('hours', $output['time']['string']);
        $this->assertStringContainsString('miles', $output['distance']['string']);
    }
}
