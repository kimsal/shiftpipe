<?php

namespace Tests\Feature;

use App\Models\Role;
use App\Models\Shift;
use App\Repositories\AccountRepository;
use App\Repositories\LocationRepository;
use App\Repositories\ShiftRepository;
use App\Repositories\SuperUserRepository;
use App\Repositories\UserRepository;
use App\Repositories\WorkerRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class ApiLocationShiftTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @return void
     */
    public function testApiAccountCreateLocation()
    {
        $repo = new SuperUserRepository();
        $user = $repo->createWithRole(['email' => 'admin@admin.com', 'name' => 'foo', 'password' => 'super'], Role::ROLE_SUPERUSER);
        $response = $this->actingAs($user, 'api')->put("/api/account/create", [
            'name' => 'account manager', 'email' => 'man@example.com', 'password' => 'password', 'accountName' => 'Account ABC'
        ]);
        $response->assertStatus(200);
        $data = json_decode($response->getContent());
        $account = (new AccountRepository())->get($data->account->id);
        $accountOwner = (new UserRepository($account))->get($data->user->id);
        session()->flush();
        $response = $this->actingAs($accountOwner, 'api')->put("/api/location/create", [
            'name' => 'sample location'
        ]);
        $response->assertStatus(200);
        $data = json_decode($response->getContent());
        $location = (new LocationRepository($accountOwner->account))->get($data->location->id);
        $this->assertEquals("sample location", $location->name);

        $response = $this->actingAs($accountOwner, 'api')->put("/api/shift/create", [
            'wallstart_date' => '2019-09-12',
            'wallstart_time' => '09:00',
            'wallend_date' => '2019-09-12',
            'wallend_time' => '14:00',
            'location_id' => $location->id
        ]);
        $response->assertStatus(200);
        $data = json_decode($response->getContent());

        $shift = (new ShiftRepository($accountOwner->account))->get($data->shift->id);
        $this->assertEquals($shift->poster->id, $accountOwner->id);
        $this->assertEquals($shift->location->id, $location->id);
        $this->assertEquals($shift->wallclock_start, "2019-09-12 09:00:00");

        $response = $this->actingAs($accountOwner, 'api')->get("/api/shift/all");
        $response->assertStatus(200);
        $data = json_decode($response->getContent());
        $this->assertCount(1, $data->shifts);

        $response = $this->actingAs($accountOwner, 'api')->put("/api/shift/create", [
            'wallstart_date' => '2019-09-12',
            'wallstart_time' => '15:00',
            'wallend_date' => '2019-09-12',
            'wallend_time' => '17:00',
            'location_id' => $location->id
        ]);
        $response->assertStatus(200);
        $data = json_decode($response->getContent());

        $response = $this->actingAs($accountOwner, 'api')->get("/api/shift/all");
        $response->assertStatus(200);
        $data = json_decode($response->getContent());
        $this->assertCount(2, $data->shifts);
    }


    public function testApiAccountShiftCreateUpdateDelete()
    {
        $repo = new SuperUserRepository();
        $user = $repo->createWithRole(['email' => 'admin@admin.com', 'name' => 'foo', 'password' => 'super'], Role::ROLE_SUPERUSER);
        $response = $this->actingAs($user, 'api')->put("/api/account/create", [
            'name' => 'account manager', 'email' => 'man@example.com', 'password' => 'password', 'accountName' => 'Account ABC'
        ]);
        $response->assertStatus(200);
        $data = json_decode($response->getContent());
        $account = (new AccountRepository())->get($data->account->id);
        $accountOwner = (new UserRepository($account))->get($data->user->id);
        session()->flush();
        $response = $this->actingAs($accountOwner, 'api')->put("/api/location/create", [
            'name' => 'sample location'
        ]);
        $response->assertStatus(200);
        $data = json_decode($response->getContent());
        $location = (new LocationRepository($accountOwner->account))->get($data->location->id);
        $this->assertEquals("sample location", $location->name);

        // let's create a shift
        $response = $this->actingAs($accountOwner, 'api')->put("/api/shift/create", [
            'wallstart_date' => '2019-09-12',
            'wallstart_time' => '09:00',
            'wallend_date' => '2019-09-12',
            'wallend_time' => '14:00',
            'location_id' => $location->id
        ]);
        $response->assertStatus(200);
        $data = json_decode($response->getContent());

        // we'll get the shift via database vs http endpoint
        $shift = (new ShiftRepository($accountOwner->account))->get($data->shift->id);
        $this->assertEquals($shift->poster->id, $accountOwner->id);
        $this->assertEquals($shift->location->id, $location->id);
        $this->assertEquals($shift->wallclock_start, "2019-09-12 09:00:00");

        $response = $this->actingAs($accountOwner, 'api')->get("/api/shift/all");
        $response->assertStatus(200);
        $data = json_decode($response->getContent());
        $this->assertCount(1, $data->shifts);

        // can get a shift by ID
        $response = $this->actingAs($accountOwner, 'api')->get("/api/shift/" . $shift->id);
        $response->assertStatus(200);
        $data = json_decode($response->getContent());
        $this->assertEquals($shift->id, $data->shift->id);

        // can we update a shift with new time/date info?
        $response = $this->actingAs($accountOwner, 'api')->post("/api/shift/update", [
            'location_id' => $shift->location_id, 'id' => $shift->id,
            'wallstart_time' => '08:00', 'wallstart_date' => '2019-09-12',
            'wallend_time' => '15:00', 'wallend_date' => '2019-09-12',
        ]);
        $response->assertStatus(200);
        $data = json_decode($response->getContent());
        $shift = (new ShiftRepository($accountOwner->account))->get($data->shift->id);
        $this->assertEquals($shift->wallclock_start, "2019-09-12 08:00:00");
        $this->assertEquals($shift->wallclock_end, "2019-09-12 15:00:00");

        // after update, do we still have the same number of shifts?
        $response = $this->actingAs($accountOwner, 'api')->get("/api/shift/all");
        $response->assertStatus(200);
        $data = json_decode($response->getContent());
        $this->assertCount(1, $data->shifts);

        // delete a shift
        $response = $this->actingAs($accountOwner, 'api')->delete("/api/shift/delete/".$shift->id);
        $response->assertStatus(200);

        // after delete, do we have one less?
        $response = $this->actingAs($accountOwner, 'api')->get("/api/shift/all");
        $response->assertStatus(200);
        $data = json_decode($response->getContent());
        $this->assertCount(0, $data->shifts);

    }

    public function testApiAccountShiftCreateTake()
    {
        $repo = new SuperUserRepository();
        $user = $repo->createWithRole(['email' => 'admin@admin.com', 'name' => 'foo', 'password' => 'super'], Role::ROLE_SUPERUSER);
        $response = $this->actingAs($user, 'api')->put("/api/account/create", [
            'name' => 'account manager', 'email' => 'man@example.com', 'password' => 'password', 'accountName' => 'Account ABC'
        ]);
        $response->assertStatus(200);
        $data = json_decode($response->getContent());
        $account = (new AccountRepository())->get($data->account->id);
        $accountOwner = (new UserRepository($account))->get($data->user->id);
        session()->flush();
        $response = $this->actingAs($accountOwner, 'api')->put("/api/location/create", [
            'name' => 'sample location'
        ]);
        $response->assertStatus(200);
        $data = json_decode($response->getContent());
        $location = (new LocationRepository($accountOwner->account))->get($data->location->id);
        $this->assertEquals("sample location", $location->name);

        // let's create a shift
        $response = $this->actingAs($accountOwner, 'api')->put("/api/shift/create", [
            'wallstart_date' => '2019-09-12',
            'wallstart_time' => '09:00',
            'wallend_date' => '2019-09-12',
            'wallend_time' => '14:00',
            'location_id' => $location->id
        ]);
        $response->assertStatus(200);
        $data = json_decode($response->getContent());

        // we'll get the shift via database vs http endpoint
        $shift = (new ShiftRepository($accountOwner->account))->get($data->shift->id);
        $this->assertEquals($shift->poster->id, $accountOwner->id);
        $this->assertEquals($shift->location->id, $location->id);
        $this->assertEquals($shift->wallclock_start, "2019-09-12 09:00:00");

        $response = $this->actingAs($accountOwner, 'api')->get("/api/shift/all");
        $response->assertStatus(200);
        $data = json_decode($response->getContent());
        $this->assertCount(1, $data->shifts);

        // can get a shift by ID
        // can't test 'take' without assigning to worker ID, don't have workers at this
        // point in test cycle
//        $worker = (new WorkerRepository($accountOwner->account))->all()->first();
//        $response = $this->actingAs($accountOwner, 'api')->post(
//            "/api/shift/take",
//            [
//                ['shiftId' => $shift->id, 'workerId' => $worker->id]
//            ]
//        );
////        dd($response);
//        $response->assertStatus(200);
//        $data = json_decode($response->getContent());
//        $this->assertEquals('OK', $data->status);

    }

}
