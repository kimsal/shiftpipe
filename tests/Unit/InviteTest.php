<?php

namespace Tests\Unit;

use App\Mail\WorkerInvite;
use App\Models\Invitation;
use App\Models\Role;
use App\Models\User;
use App\Models\Worker;
use App\Repositories\InvitationRepository;
use App\Repositories\WorkerRepository;
use App\Services\AccountService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class InviteTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp(): void
    {
        parent::setUp(); // TODO: Change the autogenerated stub
        Mail::fake();
    }

    public function testCreateAndInvite()
    {
        $service = new AccountService();
        $user = $service->createAccountWithOwnerUser("k".time()."@kimsal.com", "test user", "pass123", "Account ABC");
        $repo = new WorkerRepository($user->account);
        $name = "worker smith";
        $email = "worker@smith.com";
        $phone = "8675309";
        $invitation = $repo->createAndInvite(['name'=>$name, 'email'=>$email, 'phone'=>$phone]);
        $worker = $invitation->worker;

        $newWorker = Worker::find($worker->id);
        $this->assertCount(1, $newWorker->invitations);
        $this->assertEquals(Invitation::STATUS_SENT, $newWorker->invitations[0]->status);

        Mail::assertQueued(WorkerInvite::class, function(Mailable $mail) use ($newWorker) {
            $mail->build();
            return $mail->hasTo($newWorker->email);
        });
    }

    public function testCreateInviteAccept()
    {
        $service = new AccountService();
        $user = $service->createAccountWithOwnerUser("k".time()."@kimsal.com", "test user", "pass123", "Account ABC");
        $repo = new WorkerRepository($user->account);
        $name = "worker smith";
        $email = "worker@smith.com";
        $phone = "8675309";
        $invitation = $repo->createAndInvite(['name'=>$name, 'email'=>$email, 'phone'=>$phone]);
        $worker = $invitation->worker;

        $newWorker = Worker::find($worker->id);
        $this->assertCount(1, $newWorker->invitations);
        $this->assertEquals(Invitation::STATUS_SENT, $newWorker->invitations[0]->status);

        Mail::assertQueued(WorkerInvite::class, function(Mailable $mail) use ($newWorker) {
            $mail->build();
            return $mail->hasTo($newWorker->email);
        });

        $repo = new InvitationRepository($invitation->worker->account);
        $array = [];
        $array['name'] = 'sample user';
        $array['email'] = 'm'.time().'@kimsal.com';
        $array['password'] = "password";
        $array['confirm'] = "password";
        $repo->accept($invitation, $array);
        $invitation->refresh();
        $this->assertNotNull($invitation->worker->user->api_token);
        $this->assertNotNull($invitation->worker->user->account_id);
        $this->assertEquals(Worker::STATUS_ACTIVE, $invitation->worker->status);

        $user = User::where('email', $array['email'])->first();
        $this->assertEquals($user, $invitation->worker->user);
        $this->assertTrue($user->hasRole(Role::ROLE_WORKER));

    }
}
