<?php

namespace Tests\Unit;

use App\Models\Address;
use App\Repositories\AddressRepository;
use App\Services\AddressService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AddressTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicAddress()
    {
        $a = AddressService::create("abc");
        $this->assertNotNull($a->id);
        $this->assertEquals("abc", $a->address1);
        $address = AddressService::create('', '','','','27596');
        $this->assertNotNull($address->id);
        $this->assertNotNull($address->postal_code);
        $this->assertEquals('27596', $address->postal_code);

        $address->address1 = "address1 test";
        $newAddress = AddressService::update($address);
        $this->assertEquals($newAddress->id, $address->id);
        $this->assertEquals("address1 test", $newAddress->address1);
    }
}
