<?php

namespace Tests\Unit;

use App\Repositories\LocationRepository;
use App\Services\AccountService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class LocationTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @return void
     */
    public function testCreate()
    {
        $service = new AccountService();
        $user = $service->createAccountWithOwnerUser("k".time()."@kimsal.com", "test user", "pass123", "Account ABC");
        $repo = new LocationRepository($user->account);
        $name = "demo location";
        $location = $repo->create(['name'=>$name]);
        $this->assertEquals($name, $location->name);
    }

    public function testCreateWithAccount()
    {
        $service = new AccountService();
        $user = $service->createAccountWithOwnerUser("k".time()."@kimsal.com", "test user", "pass123", "Account ABC");
        $locRepo = new LocationRepository($user->account);
        $location = $locRepo->create(['name'=>"location 1"]);
        $this->assertEquals($user->account->id, $location->account->id);

    }
}
