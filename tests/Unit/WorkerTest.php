<?php

namespace Tests\Unit;

use App\Repositories\WorkerRepository;
use App\Services\AccountService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class WorkerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @return void
     */
    public function testCreate()
    {
        $service = new AccountService();
        $user = $service->createAccountWithOwnerUser("k".time()."@kimsal.com", "test user", "pass123", "Account ABC");
        $repo = new WorkerRepository($user->account);
        $name = "worker smith";
        $email = "worker@smith.com";
        $phone = "8675309";
        $worker = $repo->create(['name'=>$name, 'email'=>$email, 'phone'=>$phone]);
        $this->assertEquals($name, $worker->name);
        $this->assertEquals($email, $worker->email);
        $this->assertEquals($phone, $worker->phone);

        $info = $worker->toArray();
        $info['name'] = "worker smith 2";
        $repo->update($info, $worker->id);

        $workers = $repo->all();
        $this->assertGreaterThanOrEqual(1, $workers->count());
        $this->assertEquals("worker smith 2", $workers[0]->name);

        $repo->delete($workers[0]->id);
        $workers = $repo->all();
        $this->assertCount(0, $workers);
    }

}
