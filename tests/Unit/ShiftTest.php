<?php

namespace Tests\Unit;

use App\Models\Shift;
use App\Repositories\AccountRepository;
use App\Repositories\LocationRepository;
use App\Repositories\ShiftRepository;
use App\Repositories\UserRepository;
use App\Services\AccountService;
use App\Services\ShiftService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class ShiftTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @return void
     * @dataProvider shiftAfterData
     */
    public function testShiftAfter($wallclockStart, $wallclockEnd, $afterDateTime, $expected)
    {
        $account = (new AccountRepository())->create(['name'=>'account1']);
        $location = (new LocationRepository($account))->create(['name'=>'demo1', 'account_id'=>$account->id]);

        $shiftRepo = new ShiftRepository($account);
        $shift = $shiftRepo->create(['location_id'=>$location->id,
            'wallclock_start'=>$wallclockStart,
            'wallclock_end'=>$wallclockEnd,
        ]);
        $this->assertEquals($wallclockStart, $shift->wallclock_start);

        $shifts = $shiftRepo->getAfter($afterDateTime);
        $this->assertCount($expected, $shifts);
    }

    /**
     * @return void
     * @dataProvider shiftBeforeData
     */
    public function testShiftBefore($wallclockStart, $wallclockEnd, $beforeDateTime, $expected)
    {
        $account = (new AccountRepository())->create(['name'=>'account1']);
        $location = (new LocationRepository($account))->create(['name'=>'demo1', 'account_id'=>$account->id]);

        $shiftRepo = new ShiftRepository($account);
        $shift = $shiftRepo->create(['location_id'=>$location->id,
            'wallclock_start'=>$wallclockStart,
            'wallclock_end'=>$wallclockEnd,
        ]);
        $this->assertEquals($wallclockStart, $shift->wallclock_start);

        $shifts = $shiftRepo->getBefore($beforeDateTime);
        $this->assertCount($expected, $shifts);
    }


    /**
     * @return void
     * @dataProvider shiftBetweenData
     */
    public function testShiftBetween($wallclockStart, $wallclockEnd, $rangeStart, $rangeEnd, $expected)
    {
        $account = (new AccountRepository())->create(['name'=>'account1']);
        $location = (new LocationRepository($account))->create(['name'=>'demo1', 'account_id'=>$account->id]);

        $shiftRepo = new ShiftRepository($account);
        $shift = $shiftRepo->create(['location_id'=>$location->id,
            'wallclock_start'=>$wallclockStart,
            'wallclock_end'=>$wallclockEnd,
        ]);
        $this->assertEquals($wallclockStart, $shift->wallclock_start);

        $shifts = $shiftRepo->getBetween($rangeStart, $rangeEnd);
        $this->assertCount($expected, $shifts);
    }

    /**
     * @return void
     * @dataProvider shiftBeforeData
     */
    public function testShiftAccountDiscriminator($wallclockStart, $wallclockEnd, $beforeDateTime, $expected)
    {
        $account = (new AccountRepository())->create(['name'=>'account1']);
        $account2 = (new AccountRepository())->create(['name'=>'account2']);
        $location = (new LocationRepository($account))->create(['name'=>'demo1', 'account_id'=>$account->id]);
        $location2 = (new LocationRepository($account))->create(['name'=>'demo2', 'account_id'=>$account->id]);
        $location3 = (new LocationRepository($account2))->create(['name'=>'demo3', 'account_id'=>$account2->id]);

        $shiftRepo = new ShiftRepository($account);
        $shift = $shiftRepo->create(['location_id'=>$location->id,
            'wallclock_start'=>$wallclockStart,
            'wallclock_end'=>$wallclockEnd,
        ]);
        $this->assertEquals($wallclockStart, $shift->wallclock_start);

        $shifts = $shiftRepo->getBefore($beforeDateTime);
        $this->assertCount($expected, $shifts);

        $shifts = $shiftRepo->getBeforeAtLocation($beforeDateTime, $location);
        $this->assertCount($expected, $shifts);

        $shifts = $shiftRepo->getBeforeAtLocation($beforeDateTime, $location2);
        $this->assertCount(0, $shifts);

        $shiftRepo2 = new ShiftRepository($account2);
        $shifts = $shiftRepo2->getBefore($beforeDateTime);
        $this->assertCount(0, $shifts);
    }


    /**
     * @return void
     */
    public function testShiftServiceSchedule()
    {
        $accountService = new AccountService();
        $user = $accountService->createAccountWithOwnerUser("k@k.com", "test", "test", "Test Account");
        $account = $user->account;
        $location = (new LocationRepository($account))->create(['name' => 'demo1', 'account_id' => $account->id]);

        $shiftService = new ShiftService($account);
        $shift = $shiftService->schedule($location->id, "2019-12-01", "06:00",
            "2019-12-01", "14:00", $user);
        $this->assertEquals($user->id, $shift->poster_id);
        $this->assertEquals($user->id, $shift->poster->id);
    }

    /**
     * @return void
     */
    public function testShiftServiceScheduleAndTake()
    {
        $accountService = new AccountService();
        $user = $accountService->createAccountWithOwnerUser("k@k.com", "test", "test", "Test Account");
        $account = $user->account;
        $location = (new LocationRepository($account))->create(['name' => 'demo1', 'account_id' => $account->id]);

        $worker = (new UserRepository($account))->create(['email'=>'w@kimsal.com', 'name'=>'w2', 'password'=>'password']);
        $accountService->assignUserToAccount($account, $worker);

        $shiftService = new ShiftService($account);
        $shift = $shiftService->schedule($location->id, "2019-12-01", "06:00",
            "2019-12-01", "14:00", $user);
        $this->assertEquals($user->id, $shift->poster_id);
        $this->assertEquals($user->id, $shift->poster->id);

        // let's take this shift for the worker
        $newShift = $shiftService->take($shift->id, $worker->id, $user->id);
        $this->assertEquals(Shift::STATUS_TAKEN, $newShift->status);

    }

    public function shiftAfterData()
    {
        return [
            [ '2019-09-20 09:00', '2019-09-20 10:00', '2019-09-20 07:00', 1],
            [ '2019-09-20 09:00', '2019-09-20 10:00', '2019-09-20 09:00', 1],
            [ '2019-09-20 09:00', '2019-09-20 10:00', '2019-09-20 09:01', 0],
        ];
    }

    public function shiftBeforeData()
    {
        return [
            [ '2019-11-03 02:00', '2019-11-03 05:00', '2019-11-03 03:00', 1],
            [ '2019-09-20 09:00', '2019-09-20 11:00', '2019-09-20 07:00', 0],
            [ '2019-09-20 09:00', '2019-09-20 11:00', '2019-09-20 09:00', 1],
            [ '2019-09-20 09:00', '2019-09-20 11:00', '2019-09-20 09:01', 1],
            [ '2019-09-19 09:00', '2019-09-19 11:00', '2019-09-20 19:00', 1],
            [ '2019-09-19 15:00', '2019-09-19 19:00', '2019-09-20 07:00', 1],
        ];
    }

    public function shiftBetweenData()
    {
        return [
            [ '2019-09-20 09:00', '2019-09-20 10:00', '2019-09-20 00:00', '2019-09-27 00:00', 1],
            [ '2019-09-19 09:00', '2019-09-20 10:00', '2019-09-20 00:00', '2019-09-27 00:00', 0],
        ];
    }
}
