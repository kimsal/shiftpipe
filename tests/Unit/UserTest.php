<?php

namespace Tests\Unit;

use App\Repositories\SuperUserRepository;
use App\Services\AccountService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class UserTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @return void
     */
    public function testCreate()
    {
        $repo = new SuperUserRepository();
        $name = "m".time()."@kimsal.com";
        $user = $repo->create(['email'=>$name, 'name'=>$name, 'password'=>'save']);
        $this->assertEquals($name, $user->name);
    }

    /**
     * should create an account and user and link them together
     */
    public function testCreateAccountUser()
    {
        $service = new AccountService();
        $user = $service->createAccountWithOwnerUser("k".time()."@kimsal.com", "test user", "pass123", "Account ABC");
        $this->assertEquals("Account ABC", $user->account->name);
    }
}
