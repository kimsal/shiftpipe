<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/invitation/{invitation}', 'InvitationController@click')->name('invitation_click');
Route::get('/location', 'Account\LocationController@index')->name('accountlocations');
Route::get('/', 'FrontController@index')->name('front');
Route::get('/home', 'HomeController@index')->name('home');
