<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->put('/account/create', 'Api\AccountController@create');
Route::middleware('auth:api')->post('/location/update', 'Api\LocationController@update');
Route::middleware('auth:api')->delete('/location/delete/{location}', 'Api\LocationController@delete');
Route::middleware('auth:api')->put('/location/create', 'Api\LocationController@create');
Route::middleware('auth:api')->get('/location/all', 'Api\LocationController@all');
Route::middleware('auth:api')->get('/location/{location}', 'Api\LocationController@get');
Route::middleware('auth:api')->delete('/shift/delete/{shift}', 'Api\ShiftController@delete');
Route::middleware('auth:api')->post('/shift/take', 'Api\ShiftController@take');
Route::middleware('auth:api')->post('/shift/untake', 'Api\ShiftController@untake');
Route::middleware('auth:api')->post('/shift/update', 'Api\ShiftController@update');
Route::middleware('auth:api')->put('/shift/create', 'Api\ShiftController@create');
Route::middleware('auth:api')->put('/shift/createMass', 'Api\ShiftController@createMass');
Route::middleware('auth:api')->get('/shift/all', 'Api\ShiftController@all');
Route::middleware('auth:api')->get('/shift/{shift}', 'Api\ShiftController@get');

Route::middleware('auth:api')->post('/address/verify', 'Api\AddressController@verify');

Route::middleware('auth:api')->post('/worker/update', 'Api\WorkerController@update');
Route::middleware('auth:api')->post('/worker/update_address', 'Api\WorkerController@updateAddress');
Route::middleware('auth:api')->delete('/worker/delete/{worker}', 'Api\WorkerController@delete');
Route::middleware('auth:api')->put('/worker/create', 'Api\WorkerController@create');
Route::middleware('auth:api')->get('/worker/all', 'Api\WorkerController@all');
Route::middleware('auth:api')->get('/worker/{worker}', 'Api\WorkerController@get');

Route::middleware('guest:web')->post('/worker/invitation/{invitation}', 'Api\WorkerController@invitation');

Route::middleware('auth:api')->get('/me', 'Api\WorkerController@me');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
